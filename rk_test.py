import pytest

import numpy as np
from numpy.testing import (assert_, assert_allclose,
                           assert_equal)
from scipy.integrate import solve_ivp

from rk import Midpoint, RK4, BS3

def fun_zero(t, y):
    return np.zeros_like(y)

def func_spiral(t, y):
    return [np.cos(t) - y[1], np.sin(t) + y[0]]

@pytest.mark.parametrize('method', [RK4, Midpoint, BS3])
def test_rungekutta_fixed(method):
    result = solve_ivp(fun_zero, [0, 10], np.ones(3), method=method)
    assert_(result.success)
    assert_equal(result.status, 0)
    assert_allclose(result.y, 1.0, rtol=1e-15)

@pytest.mark.parametrize('method', [RK4, Midpoint, BS3])
def test_rungekutta_fixed(method):
    T = 0.1
    result = solve_ivp(func_spiral, [0, 2 * np.pi], np.zeros(2), method=method, step=T)
    # same result with julia?
    assert_(result.success)
    assert_equal(result.status, 0)
    assert_equal(len(result.t), int(2 * np.pi / T) + 2)
    t = result.t
    x = t * np.cos(t)
    y = t * np.sin(t)
    y_true = np.vstack((x, y))
    assert np.mean(np.square(y_true - result.y)) < 1e-3