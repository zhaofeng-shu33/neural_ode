using ArgParse
using Printf
using OrdinaryDiffEq
using DiffEqDevTools
using Plots

using LinearAlgebra

include("problems.jl")
include("utility.jl")

s = ArgParseSettings()
@add_arg_table s begin
  "--alg"
    default = "BS3"
  "--problem"
    default = "LotkaVolterra"
end
parsed_args = parse_args(s)

problem_name = parsed_args["problem"]
if problem_name == "LotkaVolterra"
  prob = prob_lotkavolterra
elseif problem_name == "Brusselator"
  prob = prob_brusselator
elseif problem_name == "Spiral"
  prob = prob_spiral
else
  error(string(problem_name, " not supported"))
end
sol = solve(prob, Vern7(), abstol=1/10^14, reltol=1/10^14) 
test_sol = TestSolution(sol)

alg_name = parsed_args["alg"]

if alg_name == "BS3"
  alg_instance = BS3
elseif alg_name == "Midpoint"
  alg_instance = Midpoint
elseif alg_name == "RKF4"
  alg_instance = RKF4
else
  error(string(alg_name, " not supported"))
end
coefficient, order = get_coefficient_and_order(sol, prob, alg_instance; use_prior=true)
alg_compare = alg_instance(coefficient, order)

setups = [
          Dict(:alg=>alg_compare, :adaptive=>false)
          Dict(:alg=>alg_compare, :controller=>NController())
          Dict(:alg=>alg_compare, :controller=>IController())
          Dict(:alg=>alg_compare) # default is PIController
          ]

names = [
  string(alg_name, " fixed step")
  string(alg_name, " NControl")
  string(alg_name, " IControl")
  string(alg_name, " PIControl")
  ]

abstols = 1.0 ./ 10.0 .^ (6:9)
reltols = 1.0 ./ 10.0 .^ (15:18) # ignore the influence of reltols
wp = WorkPrecisionSet(prob,abstols,reltols,setups;
                      names = names,
                      appxsol=test_sol,dense=false,
                      save_everystep=false,numruns=100,maxiters=10000000,
                      timeseries_errors=false,verbose=false)
plot(wp, title=@sprintf("Non-stiff : %s curve", problem_name))
savefig(@sprintf("build/%s_compare_%s.png", problem_name, alg_name))