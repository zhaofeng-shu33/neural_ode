import numpy as np
import torch
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy.optimize import fsolve

from rk import RK4

def plot_log_h_versue_t_max(problem, model, fig_id, save_file_name, method=RK4, err_exp=-10):        
        b = problem.theta
        N = 50
        u_0 = problem.u_0
        tmp_test_data = np.zeros([N, 5], dtype=np.float32)
        tmp_test_data[:, 0] = b
        tmp_test_data[:, 4] = err_exp
        tmax = np.linspace(problem.t_max_range[0], problem.t_max_range[1])
        tmp_test_data[:, 3] = tmax
        pred = model(torch.from_numpy(tmp_test_data))
        log_h = pred.detach().numpy().reshape(N)
        embedded_ode_func = lambda t, y: problem.ode_func(t, y, u_0, b)
        def logh_tmax(_tmax, _logh):
            result = solve_ivp(embedded_ode_func, [0, _tmax], u_0, method=method, step=np.exp(_logh[0]))
            err = problem.abs_error(u_0, b, result.t, result.y)
            return np.log(err) - err_exp
        true_log_h = []
        for i in range(50):
            tmax_i = tmax[i]
            logh_i = fsolve(lambda h: logh_tmax(tmax_i, h), -2.4)[0]
            true_log_h.append(logh_i)
        plt.figure(fig_id)
        plt.plot(np.log(tmax), log_h, label='nn fitted')
        plt.plot(np.log(tmax), true_log_h, label='true')
        plt.legend()
        plt.xlabel('log(tmax)')
        plt.ylabel('log(h)')
        plt.savefig(save_file_name)

def plot_log_h_versue_u(problem, model, fig_id, save_file_name, method=RK4, err_exp=-10):
        b = problem.theta
        N = 50
        tmp_test_data = np.zeros([N, 5], dtype=np.float32)
        tmp_test_data[:, 0] = b
        u_0_x = np.linspace(problem.u_0_range[0][0], problem.u_0_range[0][1])
        tmp_test_data[:, 1] = u_0_x
        tmp_test_data[:, 4] = err_exp
        tmax = 2 * np.pi
        tmp_test_data[:, 3] = tmax
        pred = model(torch.from_numpy(tmp_test_data))
        log_h = pred.detach().numpy().reshape(N)
        def logh_tmax(u_x, _logh):
            u_0 = [u_x, 0]
            embedded_ode_func = lambda t, y: problem.ode_func(t, y, u_0, b)
            result = solve_ivp(embedded_ode_func, [0, tmax], u_0, method=method, step=np.exp(_logh[0]))
            err = problem.abs_error(u_0, b, result.t, result.y)
            return np.log(err) - err_exp
        true_log_h = []
        for i in range(50):
            u_i = u_0_x[i]
            logh_i = fsolve(lambda h: logh_tmax(u_i, h), -2.4)[0]
            true_log_h.append(logh_i)        
        plt.figure(fig_id)
        plt.plot(u_0_x, log_h, label='nn fitted')
        plt.plot(u_0_x, true_log_h, label='true')
        # plt.ylim([-2.3, -1.7])
        plt.legend()
        plt.xlabel('log(x)')
        plt.ylabel('log(h)')
        plt.savefig(save_file_name)

def plot_log_h_versus_log_err(problem, model, fig_id, save_file_name, method=RK4):
        t_max = problem.t_max
        b = problem.theta
        N = 50
        u_0 = problem.u_0
        tmp_test_data = np.zeros([N, problem.input_dim], dtype=np.float32)
        theta_len = len(problem.theta_range)
        u_0_len = len(u_0)
        tmp_test_data[:, 0:theta_len] = b
        tmp_test_data[:, theta_len:(theta_len + u_0_len)] = u_0
        tmp_test_data[:, theta_len + u_0_len] = t_max

        plt.figure(fig_id)

        err_min = -15 # min(test_data.x[:, 4])
        err_max = -5 # max(test_data.x[:, 4])
        log_err = np.linspace(err_min, err_max)
        tmp_test_data[:, theta_len + u_0_len + 1] = log_err # range of errors
        pred = model(torch.from_numpy(tmp_test_data))
        log_h = pred.detach().numpy().reshape(N)


        embedded_ode_func = lambda t, y: problem.ode_func(t, y, u_0, b)
        true_err_list = []
        for i in range(50):
            log_h_i = log_h[i]
            result = solve_ivp(embedded_ode_func, [0, t_max], u_0, method=method, step=np.exp(log_h_i))
            err = problem.abs_error(u_0, b, result.t, result.y)
            true_err_list.append(err)
        
        log_true_error = np.log(true_err_list)
        plt.plot(log_true_error, log_h, label='true', color='red')
        # print(log_h)
        plt.plot(log_err, log_h, label='nn fitted')
        plt.legend()
        plt.xlabel('log(err)')
        plt.ylabel('log(h)')
        plt.savefig(save_file_name)

def plot_log_h_versus_b(problem, model, fig_id, save_file_name, method=RK4, err_exp=-10):
        t_max = problem.t_max
        b_range_in_use = np.linspace(problem.theta_range[0][0], problem.theta_range[0][1])
        N = 50
        u_0 = problem.u_0
        tmp_test_data = np.zeros([N, 5], dtype=np.float32)
        tmp_test_data[:, 3] = t_max
        tmp_test_data[:, 0] = b_range_in_use


        tmp_test_data[:, 4] = err_exp # range of errors
        pred = model(torch.from_numpy(tmp_test_data))
        log_h = pred.detach().numpy().reshape(N)

        
        true_b = []
        def logh_b(_b, _logh):
            embedded_ode_func = lambda t, y: problem.ode_func(t, y, u_0, _b)
            result = solve_ivp(embedded_ode_func, [0, t_max], u_0, method=method, step=np.exp(_logh[0]))
            err = problem.abs_error(u_0, _b, result.t, result.y)
            return np.log(err) - err_exp
        true_log_h = []
        for i in range(50):
            b_i = [b_range_in_use[i]]
            log_h_i = fsolve(lambda logh: logh_b(b_i, logh), -2)[0]
            true_log_h.append(log_h_i)
        
        plt.figure(fig_id)
        plt.plot(np.log(b_range_in_use), true_log_h, label='true', color='red')
        plt.plot(np.log(b_range_in_use), log_h, label='nn fitted')
        plt.legend()
        plt.xlabel('log(b)')
        plt.ylabel('log(h)')
        plt.savefig(save_file_name)
