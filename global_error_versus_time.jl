using ParameterizedFunctions, OrdinaryDiffEq,
      Plots
using DiffEqDevTools
using LinearAlgebra
using Printf
using ArgParse
include("problems.jl")


function get_global_error(;is_adaptive=false, alg_instance=BS3(), get_step_length=false, step_length=0.01, controller=nothing)
    global global_prob
    tmp_controller = controller
    
    integrator = init(global_prob, alg_instance, advance_to_tstop=true, adaptive=is_adaptive, dt=step_length, controller=tmp_controller,
                      reltol=1e-10, abstol=1e-5, progress=true, progress_steps=100
                      ) # solve the whole
   
    t0 = time_ns()
    step!(integrator)
    time_elapsed = (time_ns() - t0) / 1e9
    sol = integrator.sol
    total_t = size(sol.u)[1]
    A = zeros((total_t, 2))
    for i=1:total_t
      for j=1:2
        A[i,j] = sol.u[i][j]
      end
    end
    global true_sol
    true_sol_local = map(t-> true_sol(t), sol.t)
    x_true_sol = map(t -> t[1], true_sol_local)
    y_true_sol = map(t -> t[2], true_sol_local)
    A_true = [x_true_sol y_true_sol]
    error_list_1 = map(norm, eachslice(A - A_true, dims=1))
    if get_step_length # get the average step length
      return (global_prob.tspan[2] - global_prob.tspan[1]) / length(sol.t)
    end
    return (sol.t, error_list_1, time_elapsed)
end

s = ArgParseSettings()
@add_arg_table s begin
  "--alg"
    default = "BS3"
  "--plot"
    action = :store_true
    help = "whether to plot"
  "--problem"
    default = "Brusselator"
end
parsed_args = parse_args(s)
problem_name = parsed_args["problem"]
if problem_name == "Brusselator"
  global_prob = prob_brusselator
elseif problem_name == "Spiral"
  global_prob = prob_spiral
elseif problem_name == "SpiralInverse"
  global_prob = prob_spiral_inverse
elseif problem_name == "LotkaVolterra"
  global_prob = prob_lotkavolterra
else
  error(string(problem_name, " not supported"))
end

true_sol = solve(global_prob, Tsit5(), abstol=1/10^14, reltol=1/10^14, dense=true)

if parsed_args["plot"]
    y_1_t = map(t->t[1], true_sol.u)
    y_2_t = map(t->t[2], true_sol.u)
    plot(true_sol.t, y_1_t, label="x(t)")
    plot!(true_sol.t, y_2_t, label="y(t)")
    savefig(@sprintf("build/%s_time.pdf", problem_name))
    plot(y_1_t, y_2_t, label="f(x,y)=0")
    savefig(@sprintf("build/%s.pdf", problem_name))
    exit()
end

method_name = parsed_args["alg"]
if method_name == "BS3"
  alg_instance = BS3()
elseif method_name == "Midpoint"
  alg_instance = Midpoint()
elseif method_name == "RKF4"
  alg_instance = ExplicitRK(tableau=constructRKF4())
else
  error(string(method_name, " not supported"))
end
estimated_step_length = get_global_error(is_adaptive=true, alg_instance=alg_instance,
                               get_step_length=true)
get_global_error(alg_instance=alg_instance, step_length=estimated_step_length) # discard the results to avoid jit
get_global_error(is_adaptive=true, alg_instance=alg_instance, controller=NController()) # discard the results to avoid jit
get_global_error(is_adaptive=true, alg_instance=alg_instance, controller=IController()) # discard the results to avoid jit

# get_global_error(is_adaptive=true, use_bs3=parsed_args["bs3"], controller=PIController()) # discard the results to avoid jit

t_list_1, error_list_1, time_elapsed_1 = get_global_error(alg_instance=alg_instance, step_length=estimated_step_length) # fixed step
t_list_2, error_list_2, time_elapsed_2 = get_global_error(is_adaptive=true, alg_instance=alg_instance, controller=NController()) # adaptive
t_list_3, error_list_3, time_elapsed_3 = get_global_error(is_adaptive=true, alg_instance=alg_instance, controller=IController()) # adaptive


plot(t_list_1, error_list_1, title="global error comparison",
    label=@sprintf("fix-step %s, time %f, points %d", method_name, time_elapsed_1, length(t_list_1)), legend=:topleft)
plot!(t_list_2, error_list_2, label=@sprintf("N-adaptive %s, time %f, points %d", method_name, time_elapsed_2, length(t_list_2)))
plot!(t_list_3, error_list_3, label=@sprintf("I-adaptive %s, time %f, points %d", method_name, time_elapsed_3, length(t_list_3)))

xlabel!("t")
ylabel!("global error")
# ylabel!("step length")
savefig(@sprintf("build/error_compare_%s_%s.png", problem_name, method_name))
