using Pickle
using Test
include("models.jl")
function test_models_predict()
    a = Pickle.load(open("models/model-npy-Spiral-RK4.pickle"))
    model = build_model(a)
    data = ones(2, 5)
    predicted = model(data)
    size(predicted) == (2, 1)
end
@test test_models_predict()