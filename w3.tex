\documentclass{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bm}
\usepackage{url}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{xcolor}
\newtheorem{theorem}{Theorem}
\title{ODE Solver Step Length Auto Selection}
\author{Feng Zhao}

\begin{document}
\maketitle

Consider a standard ODE problem
\begin{align}
\frac{du}{dt} & = f(t, u) \notag\\
u(t_0) &= u_0
\end{align}
Many solvers are available to obtain numerical solutions of $u(t)$ for an interval $[t_0, t_{\max}]$.
For explicit methods, they use the following scheme to update $u_n$, which is the approximated value
of $u(t_n)$.
\begin{align}
u_{n+1} &= u_n + h_n\psi(t_n, u_n, h_n) \\
t_{n+1} &= t_n + h_n
\end{align}
\begin{description}
\item[fix-stepsize method] $h_n=h$ is pre-specified
\item[adaptive method] $h_{n+1} = g(t_n, u_n, h_n)$ is adjusted at each step.
\end{description}
In the above description of adaptive method, we focus on the simplified case when only the current state $(t_n, u_n, h_n)$
is used to obtain the new step size $h_{n+1}$. This is a local adaptive method, and relatively computational
efficient.

To obtain a good form $h_{n+1} = g(t_n, u_n, h_n)$ such that the local error
$|u(t_{n+1}) - u_{n+1}|$ decreases if new step length
is used. We need:
\begin{enumerate}
\item An error estimator of $\textrm{err}_n(h) \approx u(t_{n+1}) - u_{n+1}$, since $u(t)$ is unknown
\item A method  $h_{n+1} = g(t_n, u_n, h_n)$ to reduce $\textrm{err}_n(h) $
\end{enumerate}
In this report, we focus on the adjustment of step lengths of RK solvers.
Suppose $\psi(t_n, u_n, h_n)$ describes a RK solver with order $p$.
A common practice for $\textrm{err}_n(h) $ is to use a higher or lower order RK method to obtain $\tilde{u}_{n+1}$ to replace $u(t_{n+1})$. That is $\textrm{err}_n(h) = \tilde{u}_{n+1} - u_{n+1}
= h(\tilde{\psi}(t_n, u_n, h_n) - \psi(t_n, u_n, h_n))$ where $\tilde{\psi}(t_n, u_n, h_n)$ describes
an RK solver with order $\hat{p}$.

Another method to estimator $\textrm{err}_n(h) $ is use $u_n, u_{n+1}, u'_n = f(t_n, u_n),
u'_{n+1} = f(t_{n+1}, u_{n+1})$ to construct an interpolation curve $S(t)$ on the interval
$[t_n, t_{n+1}]$ (Hermite Cubic interpolation).
Let $\theta = \frac{t - t_n}{h} \in [0, 1]$. The interpolation curve has
the formula:
\begin{align}
    u(\theta) &= y_0 + (y_1 - y_0) (3\theta^2 - 2\theta^3)
    + \theta (\theta - 1)^2 h u'_n
    + \theta^2 (\theta - 1) h u'_{n+1} \\
    &=y_0 +  h\left[(\sum_{i=1}^s b_i k_i)(3\theta^2 - 2\theta^3)
    + \theta (\theta - 1)^2  u'_n
    + \theta^2 (\theta - 1)  u'_{n+1} \right]\label{eq:rk_s_order}
\end{align}
where \eqref{eq:rk_s_order} targets for Runge-Kutta method with $s$-stage.
Then we estimate the residual $r(t) = S'(t) - f(t, S(t))$, the
local error is chosen as $\Delta t||r(t)||$ \cite{shampine}.

\section{Traditional Method to adjust the steps}
Ceschino proposed the method using the multiplication factor $(\frac{1}{\textrm{err}_n(h)})^{1/(\tilde{p}+1)}$ to adjust
the step length, where $\tilde{p}=\min(p, \hat{p})$ is the smaller order of the two methods in comparison,
and $\textrm{err}_n(h)$ is the normalized error (normalized by the prescribed error threshold) \cite{ceschino}.
The formula can also be understood as $\textrm{err}_n(h)=O(h^{\tilde{p}+1})$
while the controlled method has $\textrm{err}_n(qh)=O(q^{\tilde{p}+1}h^{\tilde{p}+1}) \approx 1$
 %Fehlberg (1968) uses this method on a concrete scheme of RK method \cite{fehlberg}.
Then
we choose the new step length $qh$:
\begin{equation}\label{eq:icontroller}
q = \left(\frac{1}{\textrm{err}_n(h)}\right)^{1/(\tilde{p}+1)} 
= \left(\frac{\epsilon}{| \tilde{u}_{n+1} - u_{n+1}|}\right)^{\frac{1}{\tilde{p}+1}}
\end{equation}
where $\epsilon$ is the error threshold.
\begin{enumerate}
\item If $\textrm{err}_n(h) < 1$, accept the new step length $h_{n+1} = qh_n$ and update $t_{n+1} = t_n + h_n$;
\item If $\textrm{err}_n(h) > 1$, repeat this step
\end{enumerate}

My initial idea:  solve the equation $|\textrm{err}_n(qh)|=\epsilon$ to obtain the propotional parameter $q$, where $\textrm{err}_n(h) 
= h(\tilde{\psi}(t_n, u_n, h_n) - \psi(t_n, u_n, h_n))$.

\begin{enumerate}
\item When $q=0$, $|\textrm{err}_n(qh)| = 0$;
\item When $q$ is large, $|\textrm{err}_n(qh)|> \epsilon$.
\end{enumerate}

We use Newton's method to solve $|\textrm{err}_n(qh)| - \epsilon=0$
with the initial point $q_0=1$. Notice that
using high order transformation does not improve the convergence of Newton's
method.
\begin{equation}
q_{\textrm{new}} = q_0 - \frac{|\textrm{err}_n(q_0 h)| - \epsilon}
{\textrm{sgn}(\textrm{err}_n(q_0 h))
\frac{d \textrm{err}_n(qh) }{d q}
\big\vert_{q=q_0}}
\end{equation}
Below we illustrate our idea using Midpoint scheme embedding Euler.
\begin{align}
k_1 &= f(t_n, u_n) \\
k_2 & = f(t_n + \frac{1}{2} h, u_n + \frac{1}{2}h k_1) \\
\textrm{err}_n(h) & = h(k_2 - k_1) = O(h^2) \label{eq:midpoint_ini}
\end{align}
We consider the derivatives $\frac{d \textrm{err}_n(qh) }{d q}$, after some calculation,
we get:
\begin{align*}
\frac{d \textrm{err}_n(qh) }{d q}
= \textrm{err}_n(h) + \frac{qh^2}{2} [\frac{\partial f}{\partial t}(t_n + \frac{1}{2}qh, u_n +
\frac{1}{2}hk_1) 
+ k_1\frac{\partial f}{\partial u}(t_n + \frac{1}{2}qh, u_n +
\frac{1}{2}hk_1)]
\end{align*}
Notice that
    the second term when $q=1$ in the above equation is $\textrm{err}_n(h)+O(h^3)$
while $\textrm{err}_n(h) = O(h^2)$.
Therefore, we make the approximation
\begin{equation}\label{eq:approx}
\frac{d \textrm{err}_n(qh) }{d q}\big\vert_{q=1} \approx  2\textrm{err}_n(h)
\end{equation}
Then $q_{\textrm{new}} = 
1 - \frac{|\textrm{err}_n(h)| - e}{\textrm{sgn}(\textrm{err}_n(h))\frac{d \textrm{err}_n(qh) }{d q}\Big\vert_{q=1}}
\approx \frac{1}{2} + \frac{1}{2}\frac{e}{|\textrm{err}_n(h)|}$.
From this formula, if $ |\textrm{err}_n(h)| < e$, $q_{\textrm{new}} > 1$ and we increase the step length;
if $|\textrm{err}_n(h)| > e$, we decrease the step length $q_{\textrm{new}} < 1$ and repeat the current
step iteration.

Based on Theorem \ref{thm:derivative}, the iteration scheme
\begin{equation}\label{eq:midpoint_embedding}
    q_{\textrm{new}} = 
\frac{1}{2} + \frac{1}{2}\frac{e}{|\textrm{err}_n(h)|}
\end{equation}
can be generalized for other high order embedded RK methods.
\begin{theorem}\label{thm:derivative}
  Given two RK methods with order $p-1, p$ ($p\geq 2$) and updating function
  $\psi, \tilde{\psi}$.
  Let $\textrm{err}_n(h)=h(
      \tilde{\psi}(t_n, u_n, h) - \psi(t_n, u_n, h)
      )$.
  Then \begin{equation}\label{eq:approx_exact}
    \frac{d \textrm{err}_n(qh) }{d q}\big\vert_{q=1}= p\cdot \textrm{err}_n(h)(1+O(h))
    \end{equation}
\end{theorem}
\begin{proof}
    Since $\textrm{err}_n(h) = O(h^{p})$,
    we only need to show that
\begin{equation}\label{eq:deriv_q1}
  \frac{\partial [ \tilde{\psi}(t_n, u_n, qh) - \psi(t_n, u_n, q h)]}{\partial q}\big\vert_{q=1}
   =  (p-1)[\tilde{\psi}(t_n, u_n, h) - \psi(t_n, u_n, h)]+ O(h^{p})  
\end{equation}
By the property of RK method, we have
\begin{align*}
    \psi(t_n, u_n, h)
    &= f(t_n, u_n) + \sum_{i=1}^{p-2} \frac{h^{i}}{(i+1)!} \frac{d^i f(t, u_n(t))}{dt^i}
    \big\vert_{t=t_n} + \frac{d^{p-1} \psi(t_n, u_n ,h)}{d h^{p-1}} h^{p-1}  + O(h^{p}) \\
    \tilde{\psi}(t_n, u_n, h)
    &= f(t_n, u_n) + \sum_{i=1}^{p-1} \frac{h^{i}}{(i+1)!} \frac{d^i f(t, u_n(t))}{dt^i}
    \big\vert_{t=t_n}  + O(h^{p}) 
\end{align*}
Therefore,
\begin{equation*}
    \tilde{\psi}(t_n, u_n, h) - \psi(t_n, u_n, h) = g(t_n, u_n) h^{p-1} + O(h^{p})
\end{equation*}
The function $g$ is irrelevant with $t_n$. Then
\begin{align*}
    \frac{\partial [\tilde{\psi}(t_n, u_n, qh) - \psi(t_n, u_n, qh)]}{\partial q}
    &= \frac{\partial  [g(t_n, u_n) q^{p-1} h^{p-1} + O(h^{p})]}{\partial q}
    = (p-1)q^{p-2} h^{p-1} g(t_n, u_n) + O(h^{p}) \\
    &=(p-1)q^{p-2} [\tilde{\psi}(t_n, u_n, h) - \psi(t_n, u_n, h)] +  O(h^{p})
\end{align*}
Taking $q=1$ in the above equation leads \eqref{eq:deriv_q1}.
\end{proof}
Therefore, for RK method with order $p$ (estimating the error by RK method with order $p-1$).
The Newton's iteration scheme is
\begin{equation}
    q_{\textrm{new}} = 
    \frac{p-1}{p} + \frac{1}{p}\frac{e}{|\textrm{err}_n(h)|}
\end{equation}

Compared with Ceschino's method, we use a different way to calculate $q$ thanks to \eqref{eq:approx}. 
$q_{\textrm{new}} = \frac{\tilde{p}}{\tilde{p}+1} +
\frac{1}{\tilde{p}+1}\frac{e}{|\textrm{err}_n(h)|}$ does not contain the derivative information, and the computational cost is the same with that of Ceschino.
%If $\frac{\partial f}{\partial t}$
%is required, auto-differential can be used for numerical differentiation (has additional computation cost).

Can our formulation improves the performance of the solver? (it needs less time steps
to achieve the given
accuracy)
\subsection{Stability Analysis}
Let $\alpha = \frac{1}{\tilde{p} + 1}$,
consider the dynamical system
\begin{align}
    y_{n+1} &= R(h_n \lambda) y_n \notag \\
    \textrm{err}_n &= E(h_n \lambda) y_n \notag \\
    h_{n+1} &= h_n \cdot \left(1-\alpha  + \alpha \frac{\textrm{Tol}}{|\textrm{err}_n|}\right)
    \label{eq:ds}
\end{align}
where $\lambda$ comes from $y'=\lambda y$.
Using the transformation $\eta_n = \log |y_n|, \chi_n=\log h_n$,
\eqref{eq:ds} becomes
\begin{align}
    \eta_{n+1} & = \log |R(e^{\chi_n} \lambda)| + \eta_n \notag\\
    \chi_{n+1} & = \log \left(1-\alpha +  \frac{\alpha\textrm{Tol}}{e^{\eta_n} |E(e^{\chi_n}\lambda)|}\right)+ \chi_n
    \label{eq:trans_ds}
\end{align}
The fixed point satisfies
\begin{align}
    |R(z)| & = 1 \notag\\
|E(z)| e^{\eta} &= \textrm{Tol}\label{eq:ds_fix}
\end{align}
where $z=e^{\chi}\lambda$.
At the fixed point, the Jacobian of the map \eqref{eq:trans_ds} becomes (using \eqref{eq:ds_fix}
to eliminate terms)
\begin{equation}\label{eq:Cmatrix}
    C = \frac{\partial (\eta_{n+1}, \chi_{n+1})}{\partial (\eta_n, \chi_n)}
    = \begin{pmatrix}
        1 & u \\
        -\alpha & 1 - \alpha v
    \end{pmatrix}
\end{equation}
where $u = \textrm{Re}(\frac{R'(z)}{R(z)}z) $ and $v = \textrm{Re}(\frac{E'(z)}{E(z)}z)$.
The transformation has exactly the same expression with (2.32) in \cite{Hairer}.
Therefore, \textrm{NController} has the same stability behavior with 
that of \textrm{IController}.

If the safe factor $\gamma$ is taken into consideration, then
\eqref{eq:ds} becomes
\begin{equation*}
    h_{n+1} = h_n \cdot \gamma \left(1-\alpha  + \alpha \frac{\textrm{Tol}}{|\textrm{err}_n|}\right)    
\end{equation*}
In such case, the Jacobi in \ref{eq:Cmatrix}
is modified to
\begin{equation*}
    C =  \begin{pmatrix}
        1 & u \\
        -\alpha^* & 1 - \alpha^* v
    \end{pmatrix}
    \textrm{ with } \alpha^* = \gamma(\alpha - 1) + 1
\end{equation*}
This is different to the case of \textrm{IController},
where the safe factor $\gamma$ has no influence on
$C$, since it is absorbed into the tolerance term.
\subsection{PI Controller}
For stiff problems, the requirement of stability
is much stronger than that of accuracy.
As a result, the step length, which is
quite small, is chosen
on the boundary of the absolute stability
region if \texttt{IController} is adopted.
 

Iterative Formula:
\begin{equation}
    h_{n+1} = h_n \left(\frac{\textrm{Tol}}{\textrm{err}_n}\right)^{\beta_1}
    \left(\frac{\textrm{err}_{n-1}}{\textrm{Tol}}\right)^{\beta_2}
\end{equation}
Choice of Parameters (Heuristic Formula \cite{gust}, also used by \texttt{OrdinaryDiffEq}):
\begin{align}
\beta_1 &= \frac{0.7}{p} \\
\beta_2 &= \frac{0.4}{p}
\end{align}
where $p$ is the order of RK method with a higher order.

\section{embedded RK Methods}
In \eqref{eq:midpoint_ini} we have mentioned
the Midpoint method embedding Euler. Generally speaking,
we can construct a $p$ order RK Method embedding a $p-1$ order one.
For lower oder instances, we list them as follows:
\begin{enumerate}
    \item Two stage, second order embedding Euler (first order): Midpoint(2/1)
    \item Three stages, third order: Bogacki-Shampine method(3/2)
    \item Five stages, fourth order: Fehlberg (4/3) \footnote{In Fehlberg's original idea
    \cite{fehlberg}, the 4th order is used to estimate
    errors. But we can also use it to step forward, which is implemented in \texttt{OrdinaryDiffEq} package of \texttt{Julia} }
\end{enumerate}
We call the above three methods \texttt{Midpoint}, \texttt{BS3} and \texttt{RKF4} for short.
They are used throughout this paper.

%\subsection{Runge-Kutta-Fehlberg Method}
\section{Experiments}
In Julia programming language, \texttt{Midpoint} scheme is implemented using embedding Euler to estimator error while the classical RK4 uses the residual method for error estimation.
Both of their adaptive versions use \texttt{IController}
(given in \eqref{eq:icontroller}).

Example 1: Consider a planar spiral with analytical expression in polar coordinate
$r=bt$. The ODE in Cartesian system
is:
\begin{align}
\frac{dx}{dt} &= b\cos t - y \label{eq:spiral} \\    
\frac{dy}{dt} &= b\sin t + x
\end{align}
with initial condition $x(t_0)=0, y(t_0)=0, t_0=0$
We can first solve out $x=bt\cos t, y=bt\sin t$.
We also consider the case when the inverse curve starts from $(2\pi, 0)$ and approaches
the origin (i-spiral).
The figure is given in Fig. \ref{fig:spiral_curve}
\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/spiral_time.eps}
    \caption{$x(t), y(t)$}\label{fig:time_spiral_curve}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/spiral.eps}
        \caption{$f(x,y)=0$}\label{fig:positional_spiral_curve}
    \end{subfigure}
        \caption{}\label{fig:spiral_curve}
    \end{figure}

We compute four cases,
given in Fig. \ref{fig:4cases}.

How to explain the phenomenon? Initially, adaptive method performs better than the fixed step method.
The accumulation of error causes the local error estimation less precise,
and the decision to adjust steps
based on such estimation has less sense.
Therefore, the adaptive method has worse performance compared
with the fixed step alternative.
\begin{figure}[!ht]
\centering
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{fig/error_compare_midpoint.png}
\caption{Midpoint for spiral}\label{fig:com}
\end{subfigure}~
\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{fig/error_compare_rk4.png}
    \caption{RK4 for spiral}\label{fig:com2}
\end{subfigure}\\
\begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/error_compare_midpoint_inverse.png}
    \caption{Midpoint for i-spiral}\label{fig:com_inv}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/error_compare_rk4_inverse.png}
        \caption{RK4 for i-spiral}\label{fig:com2_inv}
    \end{subfigure}
    \caption{}\label{fig:4cases}
\end{figure}

However, for more complex curves such as Brusselator,
whose curve is described by
\begin{align}
    y'_1 &= 1+ y_1^2 y_2 - 4y_1 \notag\\
    y'_2 &=  3y_1 - y_1^2 y_2 \label{eq:brusselator_13}
\end{align}
with initial values $y_1(0) = 1.5, y_2(0)=3$. The time interval is $[0,20]$.

The figure of this problem is given in Fig. \ref{fig:brusselator_curve}

\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/brusselator_time.pdf}
    \caption{$x(t), y(t)$}\label{fig:time_brusselator_curve}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/brusselator.pdf}
        \caption{$f(x, y)=0$}\label{fig:positional_brusselator_curve}
    \end{subfigure}
        \caption{Brusselator simulation}\label{fig:brusselator_curve}
    \end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/error_compare_brusselator_midpoint.png}
    \caption{Midpoint for Brusselator}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/error_compare_brusselator_bs3.png}
        \caption{BS3 for Brusselator}
    \end{subfigure}
        \caption{}\label{fig:2cases}
\end{figure}

From Fig. \ref{fig:2cases}, we can see that the adaptive method is better with
some time overhead.

Below (in Fig. \ref{fig:4controller}) is more comprehensive comparison between the four controllers
using the model of \texttt{LotkaVolterra}. It can be seen that
vanilla fixed step performs better than adaptive controller.
\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/LotkaVolterra_compare_Midpoint.png}
    \caption{Midpoint for LotkaVolterra}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/LotkaVolterra_compare_BS3.png}
        \caption{BS3 for LotkaVolterra}
    \end{subfigure}
        \caption{}\label{fig:4controller}
\end{figure}
\section{Selection of Fixed Step Size}
From the above discussion we see that, fixed step method performs better for non-stiff
problems. Moreover, for some simulation tasks, which has strict time limit,
fixed step method provides guarantee to satisfy the hard constraint on time limit while
adaptive method fails to achieve this.

One advantage of adaptive step selection is that it has some guarantee of error control.
Given preferred absolute error level, the method adjusts the step to make sure the error
is controlled within the range of the prescribed error level.
For fixed step method, the same guarantee can be obtained
if we can find the explicit
mapping $\mathcal{M}$
from the error to the step size, we can just use the mapping
$\mathcal{M}$ to choose the step size.
Generally, $\mathcal{M}$ is a function of $\textrm{err}, \theta, u_0, t_{\min},
t_{\max}$: $\log h=\mathcal{M}(\log(\textrm{err}), \theta, u_0, t_{\min},
t_{\max})$. where $\theta$ parameterizes the model.
\subsection{Network architecture}
We use MLP with one hidden layer and relu as activation function.
We concatenate all parameters of \texttt{ODESolver}
into the network input $x=[\log(\textrm{err}), \theta, u_0, t_{\min},
t_{\max}]$,
and try to train the weights and biases $W_1, W_2, b_1, b_2$.
\begin{align}
    x_1 &= \textrm{relu}(W_1 x + b_1) \\
    x_2 & = \textrm{relu}(W_2 x_1 + b_2) \\
    \log h & = \log(x_2 + h_{\min}) \label{eq:logh_nn}
\end{align}
To train a neural network, we need to generate
data from a given \texttt{ODESolver}. The data
generation process is as follows:
\begin{enumerate}
    \item Generate $(h, \theta, u_0, t_{\min},
    t_{\max})$ randomly
    \item Based on the given value, using \texttt{ODESolver} to obtain $\textrm{err}$
    \item Exchange the position of $h$ and $\textrm{err}$ ($h$ is the label for this data point
    while $\textrm{err}$ is a dimension of feature)
\end{enumerate}
For fixed $\theta, t_{\min}, t_{\max}, u_0$, we expect a
monotonic mapping from $\textrm{err}$ to $h$.
The trained neural network should satisfy this property.

Besides the above mentioned agnostic model, we can also combine
the characteristics of the RK method. That is:
$\log h = k \log(\textrm{err}) + C(\tilde{x})$.
The variable $k$ does not depend on the input
$\tilde{x}$ where $\tilde{x}$ represents the
input excluding $\log(\textrm{err})$. We need
to train a neural network to learn the functional
representation of $ C(\tilde{x}) $ and the order
variable $k$.
\subsection{Experiments}

We consider an extension of \eqref{eq:spiral}
\begin{align}
    \frac{dx}{dt} &= b\cos t - y + y_0 \\    
    \frac{dy}{dt} &= b\sin t + x - x_0
\end{align}
and $u_0=[x_0, y_0], \theta=[b]$. We consider $t_{\min} = 0$
and in such case $\textrm{dim}(x) = 5$. We choose
$\textrm{dim}(x_1)=3$ and use the classical \texttt{RK4} solver, and train our
data-driven neural network
mentioned in \eqref{eq:logh_nn}.
The result is illustrated in Fig. \ref{fig:nn_h_error} and \ref{fig:nn_h_tmax}.
For this special case, we also train the
linear regression model
$\log h = \beta_1 \log (\textrm{err})
+ \beta_2 \log t_{\max} + \beta_3 \log b + \bm{\beta}_4 \cdot \log u_0 + \beta_5$
(where $\bm{\beta}_4$ is a vector with dimension 2.) We require that $u_0>0$ for our data.

For these three models, their average $\ell_2$ losses are listed in Table
\ref{tab:spiral_p}.
\begin{table}[!ht]
    \centering
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        Method(RK4) & Number of variables & Training loss & Test loss & $R^2$ (Test data)\\
        \hline
        data-driven NN &22 & 7.0e-2 & 7.7e-2 & 0.93\\
        \hline
        with-prior NN & 13 & 2.8e-3 & 3.3e-3 & 0.997\\
        \hline
        linear regression & 6 & 2.7e-4& 3.2e-4 & 0.9998\\
        \hline
    \end{tabular}
    \caption{Spiral Problem}\label{tab:spiral_p}
\end{table}
For this specific problem, $\log h$ is irrelevant with $u_0$.
This relationship can also be learned by the data-driven NN.
Since the figure $\log h(u_0)$ is not informative,
we omit it in Fig. \ref{fig:nn}.

\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/nn_data-driven_h_error.eps}
    \caption{data-driven NN fitted $\log h(\log\textrm{err})$\\
    for $t_{\max}=2\pi,b=1,u_0=(0,0)$}\label{fig:nn_h_error}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/nn_data-driven_tmax_error.eps}
        \caption{\scriptsize{data-driven NN fitted $\log h(\log t_{\max})$}
        \\
    for $\log(\textrm{err})=-8,b=1,u_0=(0,0)$
    }\label{fig:nn_h_tmax}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{fig/nn_data-driven_b_error.eps}
        \caption{data-driven NN fitted $\log h(\log b)$\\
        for \scriptsize{$t_{\max}=2\pi,\log(\textrm{err})=-8,u_0=(0,0)$}}
        \label{fig:_nn_b_error}
        \end{subfigure}

    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{fig/nn_with-prior_h_error.eps}
        \caption{with prior NN fitted $\log h(\log\textrm{err})$\\
        for $t_{\max}=2\pi,b=1,u_0=(0,0)$}\label{fig:with-prior_nn_h_error}
        \end{subfigure}~
        \begin{subfigure}[b]{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{fig/nn_with-prior_tmax_error.eps}
            \caption{with prior NN fitted $\log h(\log t_{\max})$\\
            for $\log(\textrm{err})=-8,b=1,u_0=(0,0)$
            }\label{fig:with-prior_nn_h_tmax}
        \end{subfigure}~
        \begin{subfigure}[b]{0.45\textwidth}
            \includegraphics[width=\textwidth]{fig/nn_with-prior_b_error.eps}
            \caption{with prior NN fitted $\log h(\log b)$\\
            for \scriptsize{$t_{\max}=2\pi,\log(\textrm{err})=-8,u_0=(0,0)$}}
            \label{fig:with-prior_nn_b_error}
            \end{subfigure}

            \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth]{fig/nn_linear_h_error.eps}
                \caption{with prior NN fitted $\log h(\log\textrm{err})$\\
                for $t_{\max}=2\pi,b=1,u_0=(0,0)$}\label{fig:linear_nn_h_error}
                \end{subfigure}~
                \begin{subfigure}[b]{0.45\textwidth}
                    \centering
                    \includegraphics[width=\textwidth]{fig/nn_linear_tmax_error.eps}
                    \caption{with prior NN fitted $\log h(\log t_{\max})$\\
                    for $\log(\textrm{err})=-8,b=1,u_0=(0,0)$
                    }\label{fig:linear_nn_h_tmax}
                \end{subfigure}~
                \begin{subfigure}[b]{0.45\textwidth}
                    \includegraphics[width=\textwidth]{fig/nn_linear_b_error.eps}
                    \caption{with prior NN fitted $\log h(\log b)$\\
                    for \scriptsize{$t_{\max}=2\pi,\log(\textrm{err})=-8,u_0=(0,0)$}}
                    \label{fig:linear_nn_b_error}
                    \end{subfigure}
        \caption{}\label{fig:nn}
\end{figure}

We also test the method-agnostic approach with data-driven network
structure. The RK method is treated as categorical feature and
transformed by one-hot encoding. Surprisingly, the performance
is improved compared with the case when restricting to
a single method. We call the method \texttt{All}, which combines \texttt{RKF4}, \texttt{BS3}
and \texttt{Midpoint}. The result is reported in Table \ref{tab:spiral_method_compare}.
\begin{table}[!ht]
    \centering
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        Method & Training loss & Test loss & $R^2$ (Test data)\\
        \hline
        RKF4  & 0.029 & 0.042 & 0.965\\
        \hline
        BS3  & 0.091 & 0.080 & 0.933\\
        \hline
        Midpoint & 0.182& 0.142 & 0.879\\
        \hline
        All & 0.011 & 0.013 & 0.989 \\
        \hline
    \end{tabular}
    \caption{Spiral Problem with data-driven NN structure}\label{tab:spiral_method_compare}
\end{table}
Another experiment uses the model of Lotka-Volterra \cite{lotka},
which is described as follows:
\begin{align*}
    \frac{\mathrm dN_1}{\mathrm dt} &= N_1(\epsilon_1-\gamma_1 N_2) \\
    \frac{\mathrm dN_2}{\mathrm dt} &= -N_2(\epsilon_2-\gamma_2 N_1) 
\end{align*}
A example of the time curve and its trajectory in phase space:
\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/LotkaVolterra_time.pdf}
    \caption{$x(t), y(t)$}\label{fig:time_LotkaVolterra_curve}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/LotkaVolterra.pdf}
        \caption{$f(x, y)=0$}\label{fig:positional_LotkaVolterra_curve}
    \end{subfigure}
        \caption{LotkaVolterra simulation}\label{fig:LotkaVolterra_curve}
    \end{figure}

For this model, $\theta=[\epsilon_1, \gamma_1, \epsilon_2, \gamma_2]$.

\begin{table}[!ht]
    \centering
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        Method(RK4) & Number of variables & Training loss & Test loss & $R^2$ (Test data)\\
        \hline
        data-driven NN &28 & 4.76 &  4.21 & -3.3\\
        \hline
        with-prior NN & 19 & 0.13 & 0.15 & 0.84\\
        \hline
        linear regression & 9 & 0.27 & 0.22 & 0.77\\
        \hline
    \end{tabular}
    \caption{LotkaVolterra Problem}
\end{table}

As a final example we use the model of Brusselator. The ODE is given by
\begin{align}
    y'_1 &= A+ y_1^2 y_2 - (B+1)y_1 \notag\\
    y'_2 &=  By_1 - y_1^2 y_2 \label{eq:brusselator}
\end{align}
which generalizes \eqref{eq:brusselator_13}

\begin{table}[!ht]
    \centering
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        Method(RK4) & Number of variables & Training loss & Test loss & $R^2$ (Test data)\\
        \hline
        data-driven NN & 25 & 0.86 &  1.18 & -0.22\\
        \hline
        with-prior NN & 16 & 0.037 & 0.044 & 0.95\\
        \hline
        linear regression & 7 & 0.058 & 0.063 & 0.93\\
        \hline
    \end{tabular}
    \caption{Brusselator Problem}
\end{table}
\section{PIController parameter identification}
A natural question arises how to choose the parameters $\beta_1,\beta_2$
of \texttt{PIController}.
Generally speaking, it's a tradeoff between the response time (minimizing
the number of reject steps or total number of function evaluation)
and decreasing the sensitivity against the noise.

Example:
\begin{align}
    y'_1 &= -2000  (\cos(t)  y_1 + \sin(t)  y_2 + 1)\notag \\
    y'_2 &= -2000  (-\sin(t)  y_1 + \cos(t)  y_2 + 1) \notag\\
     y_1 &= -1, y_2 = -1,  t \in [0, \frac{\pi}{2}]
\end{align}
\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/IController_stiff.eps}
    \caption{\texttt{IController}}\label{fig:IController_stiff}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/PIController_stiff.eps}
        \caption{\texttt{PIController}}\label{fig:PIController_stiff}
    \end{subfigure}
        \caption{BS3}\label{fig:sensitivity}
    \end{figure}

In Fig. \ref{fig:IController_stiff}, the numerical solutions have oscillations.
For \texttt{PIController}, if we choose $\beta_2$ too small. It reduces to \texttt{IController}.

Empircally speaking, the sensitivity contributes to
the global error, so that the problem reduces to
time and global error tradeoff.
Our global is to learn the curve and focus
on the following formulation:
\begin{align}
    \min& E(\beta_1, \beta_2) \notag\\
    s.t. \,  & T \leq T^* \label{eq:E}
\end{align}
\begin{enumerate}
    \item Obtain the lower bound of $T^*$
    \item Sampling several $T^*$ and solve \eqref{eq:E} using GA \footnote{Equivalent to obtain the Pareto frontier for $(T, E)$}
    \item Fitting the curve 
\end{enumerate}

Notice that if $\beta_1, \beta_2$ are poorly
chosen, the solver is not stable and at some step it enters the rejection loop.
That is, the steps performed are infinite. To solve this
issue, we can set a upper limit for the maximal number
of function calling.
From my literature review, machine learning method to achieve this goal can be
roughly divided into three genres:
\begin{enumerate}
    \item Genetic Algorithm (or other Black Box optimization)
    \item Reinforcement Learning (treating $\beta_1, \beta_2$ as policy parameter)
    \item Neural ODE (adjoint sensitivity method)
\end{enumerate}
\subsection{Genetic Algorithm Approach}
Totally black box optimization method. Advantage: does not need gradient information.

We adopt a sequential initialization strategy which solves a series of
optimization problems in \eqref{eq:E} for increasing $T^*$.

\subsection{Reinforcement Learning Approach}
Some articles \cite{robot} propose the method of
tuning PID parameters for complex model by PILCO.
Inspired by PILCO, we propose a shallow strategy to obtain
optimal parameters $\beta_1, \beta_2$ by defining a 
differentiable loss function $L(\beta_1, \beta_2)$
and using the available data to train the RL model.

Notice that in RL approach, we still treat the environment
(ODESolver) as a black box. For $n\geq 1$, the state vector is
$s_n = (t_n, u_n, e_n)$ where
$e_n$ is the estimated error.

$h_n$, the step length at time
$t_{n}$ is applied to the system
by the controller.
Then $s_{n+1} = f(s_n, h_n)$.
The form of $f$ is explicitly given by:
\begin{align}
    t_{n+1} &= t_n + h_n \\
    u_{n+1} &= u_n + h_n \psi(t_n, u_n, h_n) \\
    e_{n+1} &= h_n (\psi(t_n, u_n, h_n) - \tilde{\psi}(t_n, u_n, h_n))
\end{align}
The RL policy $h_n = \pi(s_n, \beta_1, \beta_2)$ is given by
\begin{align}
\hat{h}_n &= h_{n-1}\left(\frac{\textrm{Tol}}{e_n}\right)^{\beta_1}
\left(\frac{e_{n-1}}{\textrm{Tol}}\right)^{\beta_2} \\
\hat{e}_{n+1} &= \hat{h}_n (\psi(t_n, u_n, \hat{h}_n) - \tilde{\psi}(t_n, u_n, \hat{h}_n))\\
\hat{h} &=  h_{n-1}\left(\frac{e_{n-1}}{\textrm{Tol}}\right)^{\beta_2}
\left[\mathbf{1}[e_n < \textrm{Tol}]\left(\frac{\textrm{Tol}}{e_n}\right)^{\beta_1}
+\mathbf{1}[e_n > \textrm{Tol}]\left(\frac{\textrm{Tol}}{\hat{e}_{n+1}}\right)^{\beta_1}\right]
\label{eq:h_hat}
\end{align}
After obtaining the data $(\bar{s}_n, \bar{h}_n, \bar{\hat{e}}_{n+1})$\footnote{We distinguish the data and variable by adding a bar over sampled data},
the loss at each step
is given by the global error $\ell_n = (u(t_n) - u_n)^2$ where $u(t)$ is the
exact solution of the ODE problem.

Notice that we also record the rejected error estimation $\bar{\hat{e}}_{n+1}$,
which makes \eqref{eq:h_hat} much simpler.

\eqref{eq:h_hat} is only approximately true. If we introduce safety factor $\gamma$, with large
probability that the decision of reject at most
happens once at each time step.

Considering the time constraint $T^*$, we add
a penalized term $\alpha>0$.
Then the total loss is expressed as:
\begin{align}
    L(\beta_1, \beta_2)
    = \alpha(t_{\max} - \sum_{i=1}^{\min(n, T^*)}h_i) 
    + \sum_{i=1}^{\min(n, T^*)} \ell_i
\end{align}
In the above formula, denote $\bar{e}'_n = \min\{\bar{e}_n, \bar{\hat{e}}_{n+1} \}$
$h_i = \bar{h}_{i-1}\left(\frac{\textrm{Tol}}{\bar{e}'_n}\right)^{\beta_1}\left(\frac{\bar{e}_{n-1}}{\textrm{Tol}}\right)^{\beta_2}
$
and $u_i = \bar{u}_{i-1} + h_{i-1} \psi(\bar{t}_{i-1}, \bar{u}_{i-1}, \bar{h}_{i-1})$.
Thus we don't need to know the inner structure
of $\psi,\tilde{\psi}$ to take the derivative.
\bibliographystyle{plain}
\begin{thebibliography}{9}
\bibitem{ceschino} Ceschino, Francis. "Modification de la longueur du pas dans l’intégration numérique par les méthodes à pas liés." Chiffres 4 (1961): 101-106.
\bibitem{fehlberg} Fehlberg, Erwin. Low-order classical Runge-Kutta formulas with stepsize control and their application to some heat transfer problems. Vol. 315. National aeronautics and space administration, 1969.
\bibitem{shampine} Shampine, Lawrence F. "Error estimation and control for ODEs." Journal of Scientific Computing 25.1 (2005): 3-16.
\bibitem{lotka} \url{https://de.wikipedia.org/wiki/Lotka-Volterra-Gleichungen}
\bibitem{Hairer} Wanner, Gerhard, and Ernst Hairer. Solving ordinary differential equations II. Vol. 375. Springer Berlin Heidelberg, 1996.
\bibitem{robot} Doerr, Andreas, et al. "Model-based policy search for automatic tuning of multivariate PID controllers." 2017 IEEE International Conference on Robotics and Automation (ICRA). IEEE, 2017.
\bibitem{gust} Gustafsson, Kjell. "Control theoretic techniques for stepsize selection in explicit Runge-Kutta methods." ACM Transactions on Mathematical Software (TOMS) 17.4 (1991): 533-554.
\end{thebibliography}
\end{document}
