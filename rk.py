# implement fixed step Runge Kutta solver
import numpy as np
from scipy.integrate import OdeSolver
from scipy.integrate._ivp.common import (validate_max_step, select_initial_step,
                     norm, warn_extraneous, validate_first_step)
from scipy.integrate._ivp.rk import rk_step

class RungeKuttaFixed(OdeSolver):
    C: np.ndarray = NotImplemented
    A: np.ndarray = NotImplemented
    B: np.ndarray = NotImplemented
    order: int = NotImplemented
    error_estimator_order: int = NotImplemented
    n_stages: int = NotImplemented

    def __init__(self, fun, t0, y0, t_bound, step=1.0,
                 vectorized=False, **extraneous):
        warn_extraneous(extraneous)
        super().__init__(fun, t0, y0, t_bound, vectorized,
                         support_complex=True)
        self.y_old = None
        
        # self.rtol, self.atol = validate_tol(rtol, atol, self.n)
        self.f = self.fun(self.t, self.y)
        self.h_abs = validate_max_step(step)
        self.K = np.empty((self.n_stages + 1, self.n), dtype=self.y.dtype)
        self.h_previous = None

    def _estimate_error(self, K, h):
        return np.dot(K.T, self.E) * h

    def _estimate_error_norm(self, K, h, scale):
        return norm(self._estimate_error(K, h) / scale)

    def _step_impl(self):
        t = self.t
        y = self.y

        h_abs = self.h_abs

        h = h_abs * self.direction
        t_new = t + h

        if self.direction * (t_new - self.t_bound) > 0:
            t_new = self.t_bound

        h = t_new - t
        h_abs = np.abs(h)

        y_new, f_new = rk_step(self.fun, t, y, self.f, h, self.A,
                                self.B, self.C, self.K)

        self.h_previous = h
        self.y_old = y

        self.t = t_new
        self.y = y_new

        self.h_abs = h_abs
        self.f = f_new

        return True, None

    def _dense_output_impl(self):
       raise NotImplemented("")

class RK4(RungeKuttaFixed):
    """Explicit Runge-Kutta method of order 4.
    """
    order = 4
    n_stages = 4
    C = np.array([0, 1/2, 1/2, 1])
    A = np.array([
        [0, 0, 0, 0],
        [1/2, 0, 0, 0],
        [0, 1/2, 0, 0],
        [0, 0, 1, 0]
    ])
    B = np.array([1/6, 1/3, 1/3, 1/6])

class Midpoint(RungeKuttaFixed):
    order = 2
    n_stages = 2
    C = np.array([0, 0.5])
    A = np.array([
        [0, 0],
        [1/2, 0]
    ])
    B = np.array([0, 1.0])

class BS3(RungeKuttaFixed):
    order = 3
    n_stages = 3
    C = np.array([0, 0.5, 0.75])
    A = np.array([
        [0, 0, 0],
        [1/2, 0, 0],
        [0, 0.75, 0]
    ])
    B = np.array([2/9, 1/3, 4/9])