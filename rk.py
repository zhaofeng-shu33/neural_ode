# implement fixed step Runge Kutta solver
import numpy as np
from scipy.integrate import OdeSolver
from scipy.integrate._ivp.common import (validate_max_step, validate_tol, select_initial_step,
                     norm, warn_extraneous, validate_first_step)
from scipy.integrate._ivp.rk import rk_step, RkDenseOutput
from scipy.integrate._ivp.rk import SAFETY, MIN_FACTOR, MAX_FACTOR

class RungeKuttaAdaptive(OdeSolver):
    C: np.ndarray = NotImplemented
    A: np.ndarray = NotImplemented
    B: np.ndarray = NotImplemented
    E: np.ndarray = NotImplemented
    P: np.ndarray = NotImplemented
    order: int = NotImplemented
    error_estimator_order: int = NotImplemented
    n_stages: int = NotImplemented
    TOO_MANY_EVAL = "Function Evaluation exceeds the maximal time"
    def __init__(self, fun, t0, y0, t_bound, step=None, adaptive=True,
                 max_step=np.inf,
                 rtol=1e-3, atol=1e-6, first_step=None, beta_1=None,
                 beta_2=None, controller='IController', max_nfev = np.inf,
                 vectorized=False, **extraneous):
        warn_extraneous(extraneous)
        super().__init__(fun, t0, y0, t_bound, vectorized,
                         support_complex=True)
        self.y_old = None
        if step is not None:
            self.adaptive = False
        else:
            self.adaptive = adaptive
            self.max_nfev = max_nfev
        if controller == 'PIController':
            self.is_pi_control = True
            if beta_1 is None:
                beta_1 = 0.7 / (self.error_estimator_order + 1)
            if beta_2 is None:
                beta_2 = 0.4 / (self.error_estimator_order + 1)
            self.beta_1 = beta_1
            self.beta_2 = beta_2
            self.previous_error_norm = 1.0
        else:
            self.is_pi_control = False
        self.f = self.fun(self.t, self.y)

        if self.adaptive:
            self.max_step = validate_max_step(max_step)
            self.rtol, self.atol = validate_tol(rtol, atol, self.n)
            if first_step is None:
                self.h_abs = select_initial_step(
                    self.fun, self.t, self.y, self.f, self.direction,
                    self.error_estimator_order, self.rtol, self.atol)
            else:
                self.h_abs = validate_first_step(first_step, t0, t_bound)
            self.error_exponent = -1 / (self.error_estimator_order + 1)
        else:
            self.h_abs = validate_max_step(step)
        
        self.K = np.empty((self.n_stages + 1, self.n), dtype=self.y.dtype)
        self.h_previous = None
        if self.order >= 3: # construct P on the fly
            self.P = np.zeros([self.K.shape[0], 3])
            self.P[0, 0] = 1
            self.P[-1, 1] = -1
            self.P[-1, 2] = 1
            self.P[:-1, 1] = 3 * self.B
            self.P[:-1, 2] = -2 * self.B
            self.P[0, 1] -= 2
            self.P[0, 2] += 1

    def _estimate_error(self, K, h):
        return np.dot(K.T, self.E) * h

    def _estimate_error_norm(self, K, h, scale):
        return norm(self._estimate_error(K, h) / scale)

    def _step_impl(self):
        t = self.t
        y = self.y

        h_abs = self.h_abs

        if self.adaptive:
            max_step = self.max_step
            rtol = self.rtol
            atol = self.atol

            min_step = 10 * np.abs(np.nextafter(t, self.direction * np.inf) - t)

            if self.h_abs > max_step:
                h_abs = max_step
            elif self.h_abs < min_step:
                h_abs = min_step
            else:
                h_abs = self.h_abs

            step_rejected = False
        step_accepted = False
        while not step_accepted:
            if self.adaptive and h_abs < min_step:
                return False, self.TOO_SMALL_STEP
            elif self.adaptive and self.nfev > self.max_nfev:
                return False, self.TOO_MANY_EVAL
            else:
                step_accepted = True
            h = h_abs * self.direction
            t_new = t + h

            if self.direction * (t_new - self.t_bound) > 0:
                t_new = self.t_bound

            h = t_new - t
            h_abs = np.abs(h)

            y_new, f_new = rk_step(self.fun, t, y, self.f, h, self.A,
                                    self.B, self.C, self.K)
            if self.adaptive:
                scale = atol + np.maximum(np.abs(y), np.abs(y_new)) * rtol
                error_norm = self._estimate_error_norm(self.K, h, scale)
                if error_norm != 0:
                    if self.is_pi_control:
                        propose_q = error_norm ** (-self.beta_1)
                        propose_q *= self.previous_error_norm ** self.beta_2
                    else:
                        propose_q = error_norm ** self.error_exponent
                if error_norm < 1: # increase the step length
                    if error_norm == 0:
                        factor = MAX_FACTOR
                    else:
                        factor = min(MAX_FACTOR,
                                    SAFETY * propose_q)

                    if step_rejected:
                        factor = min(1, factor)

                    h_abs *= factor

                    step_accepted = True
                    self.previous_error_norm = error_norm
                else: # decrease the step length
                    h_abs *= max(MIN_FACTOR,
                                SAFETY * propose_q)
                    step_rejected = True

        self.h_previous = h
        self.y_old = y

        self.t = t_new
        self.y = y_new

        self.h_abs = h_abs
        self.f = f_new

        return True, None

    def _dense_output_impl(self):
        Q = self.K.T.dot(self.P)
        return RkDenseOutput(self.t_old, self.t, self.y_old, Q)

class RK4(RungeKuttaAdaptive):
    """Explicit Runge-Kutta method of order 4.
       does not support adaptive step size
    """
    order = 4
    n_stages = 4
    C = np.array([0, 1/2, 1/2, 1])
    A = np.array([
        [0, 0, 0, 0],
        [1/2, 0, 0, 0],
        [0, 1/2, 0, 0],
        [0, 0, 1, 0]
    ])
    B = np.array([1/6, 1/3, 1/3, 1/6])
    # Hermite Cubic polynomial

class RKF4(RungeKuttaAdaptive):
    # mentioned in the following article
    # Low-order classical Runge-Kutta formulas with stepsize control and their application to some heat transfer problems
    order = 4
    n_stages = 5
    error_estimator_order = 3
    C = np.array([0, 1/4, 4/9, 6/7, 1])
    A = np.array([
        [0, 0, 0, 0, 0],
        [1/4, 0, 0, 0, 0],
        [4/81, 32/81, 0, 0, 0],
        [57/98, -432/343, 1053/686, 0, 0],
        [1/6,0,27/52,49/156,0]
    ])
    B = np.array([43/288, 0, 243/416, 343/1872, 1/12])
    E = np.array([-5/288, 0, 27/416, -245/1872, 1/12, 0])

class Midpoint(RungeKuttaAdaptive):
    order = 2
    n_stages = 2
    error_estimator_order = 1
    C = np.array([0, 0.5])
    A = np.array([
        [0, 0],
        [1/2, 0]
    ])
    B = np.array([0, 1.0])
    # linear interpolation
    P = np.array([[0.0], [1.0], [0.0]])
    E = np.array([-1.0, 1.0, 0])
class BS3(RungeKuttaAdaptive):
    order = 3
    error_estimator_order = 2
    n_stages = 3
    C = np.array([0, 0.5, 0.75])
    A = np.array([
        [0, 0, 0],
        [1/2, 0, 0],
        [0, 0.75, 0]
    ])
    B = np.array([2/9, 1/3, 4/9])
    E = np.array([5/72, -1/12, -1/9, 1/8])

class DOPRI5(RungeKuttaAdaptive):
    order = 5
    error_estimator_order = 4
    n_stages = 6
    C = np.array([0, 1/5, 3/10, 4/5, 8/9, 1])
    A = np.array([
        [0, 0, 0, 0, 0],
        [1/5, 0, 0, 0, 0],
        [3/40, 9/40, 0, 0, 0],
        [44/45, -56/15, 32/9, 0, 0],
        [19372/6561, -25360/2187, 64448/6561, -212/729, 0],
        [9017/3168, -355/33, 46732/5247, 49/176, -5103/18656]
    ])
    B = np.array([35/384, 0, 500/1113, 125/192, -2187/6784, 11/84])
    E = np.array([-71/57600, 0, 71/16695, -71/1920, 17253/339200, -22/525,
                  1/40])