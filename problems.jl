
using ParameterizedFunctions
f = @ode_def_bare LotkaVolterra begin
  dx = a*x - b*x*y
  dy = -c*y + d*x*y
end a b c d
f2 = @ode_def_bare Spiral begin
  dx = sig * (b * cos(t) - y)
  dy = b * sin(t) + sig * x
end b sig

function Brusselator!(dy, y, p, t)
  dy[1]= 1 + y[1]^2 * y[2] - 4 * y[1]
  dy[2] = 3 * y[1] - y[1]^2 * y[2]
  # [dy1,dy2]
end

function get_lotkavolterra_problem()

  p = [1.5,1,3,1]
  tspan = (0.0,10.0)
  u0 = [1.0,1.0]
  return ODEProblem(f,u0,tspan,p)
end

function get_spiral_problem(direction=1.0)
 
  p = [1.0, direction]
  tspan = (0.0, 2 * pi)
  u0 = [pi * (1 - direction), 0.0]
  return ODEProblem(f2,u0,tspan,p)
end
function get_brusselator_problem()
  tspan = (0.0,20.0)
  u0 = [1.5, 3]
  return ODEProblem(Brusselator!, u0, tspan)
end
prob_lotkavolterra = get_lotkavolterra_problem()

prob_spiral = get_spiral_problem(1.0)

prob_spiral_inverse = get_spiral_problem(-1.0)

prob_brusselator = get_brusselator_problem()