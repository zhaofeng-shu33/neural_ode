
using ParameterizedFunctions
f = @ode_def_bare LotkaVolterra begin
  dx = a*x - b*x*y
  dy = -c*y + d*x*y
end a b c d
f2 = @ode_def_bare Spiral begin
  dx = b * cos(t) - y
  dy = b * sin(t) + x
end b

f2_i = @ode_def_bare SpiralInverse begin
  dx = -1.0 * (b * cos(t) - y)
  dy = b * sin(t) - x
end b

f3 =  @ode_def_bare Brusselator begin
  dx = A + x^2 * y - (B + 1) * x
  dy = B * x - x^2 * y
end A B


function get_lotkavolterra_problem()

  p = [1.5,1,3,1]
  tspan = (0.0,10.0)
  u0 = [1.0,1.0]
  return ODEProblem(f,u0,tspan,p)
end

function get_spiral_problem()
 
  p = [1.0]
  tspan = (0.0, 2 * pi)
  u0 = [0.0, 0.0]
  return ODEProblem(f2,u0,tspan,p)
end

function get_inverse_spiral_problem()
 
  p = [1.0]
  tspan = (0.0, 2 * pi)
  u0 = [2 * pi, 0.0]
  return ODEProblem(f2_i,u0,tspan,p)
end

function get_brusselator_problem()
  p = [1, 3.0]
  tspan = (0.0,20.0)
  u0 = [1.5, 3]
  return ODEProblem(f3, u0, tspan, p)
end
prob_lotkavolterra = get_lotkavolterra_problem()

prob_spiral = get_spiral_problem()

prob_spiral_inverse = get_inverse_spiral_problem()

prob_brusselator = get_brusselator_problem()