using OrdinaryDiffEq
using DiffEqDevTools
using Pickle # https://github.com/chengchingwen/Pickle.jl
using Printf

function get_coefficient_and_order(test_sol, ode_prob, alg; use_prior=false)
    if contains(String(Symbol(ode_prob.f)), "Spiral") && use_prior # only support linear model
      alg_name = String(Symbol(alg))
      model_file = @sprintf("models/model-npy-Spiral-%s.pickle", alg_name)
      tmp_dic = Pickle.load(open(model_file))
      spiral_beta_1 = tmp_dic["variable"]
      spiral_beta_2 = tmp_dic["linear_stack.0.weight"][1]
      spiral_beta_3 = tmp_dic["linear_stack.0.bias"][1]
      order = 1 / spiral_beta_1
      input = vcat([ode_prob.p[1]], ode_prob.u0 + [0.001, 0.001], [ode_prob.tspan[2]])
      tmp_val = @. spiral_beta_2 * log(abs(input))
      coefficient = exp(-(sum(tmp_val) + spiral_beta_3) / spiral_beta_1)
    else
      h = [1e-3, 5e-3, 0.01]
      abs_err = []
      # make sure the correlation coefficient is larger than 0.98
      for i=1:3
        sol = solve(ode_prob, alg(), dt=h[i], adaptive=false)
        e_i = appxtrue(sol, test_sol).errors[:final]
        append!(abs_err, e_i)
      end
      res = hcat(ones(3), @. log(h)) \ (@. log(abs_err))
      order = res[2]
      coefficient = exp(res[1])
    end 
    (coefficient, order)
  end