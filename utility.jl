using OrdinaryDiffEq
using DiffEqDevTools
using Pickle # https://github.com/chengchingwen/Pickle.jl
using Printf
include("models.jl")
function RKF4(coefficient, order)
  return ExplicitRK(constructRKF4(), coefficient, order)
end
function get_coefficient_and_order(test_sol, ode_prob, alg; use_prior=false)
    if contains(String(Symbol(ode_prob.f)), "Spiral")
      problem_name = "Spiral"
    elseif contains(String(Symbol(ode_prob.f)), "Brusselator")
      problem_name = "Brusselator"
    elseif contains(String(Symbol(ode_prob.f)), "LotkaVolterra")
      problem_name = "LotkaVolterra"
    else
      problem_name = ""
    end
    if use_prior # only support linear model
      alg_name = String(Symbol(alg))
      model_file = @sprintf("models/model-npy-%s-%s.pickle", problem_name, alg_name)
      tmp_dic = Pickle.load(open(model_file))
      model = build_model(tmp_dic)
      input_others = transpose(vcat(ode_prob.p, ode_prob.u0, [ode_prob.tspan[2]]))
      log_abs_err = [-10, 5]
      input_whole = vcat(hcat(input_others, log_abs_err[1]), hcat(input_others, log_abs_err[2]))
      log_h = model(input_whole)
      res = hcat(ones(2), log_h) \ log_abs_err
    else
      h = [1e-3, 5e-3, 0.01]
      abs_err = []
      # make sure the correlation coefficient is larger than 0.98
      for i=1:3
        sol = solve(ode_prob, alg(), dt=h[i], adaptive=false)
        e_i = appxtrue(sol, test_sol).errors[:final]
        append!(abs_err, e_i)
      end
      res = hcat(ones(3), @. log(h)) \ (@. log(abs_err))
    end
    order = res[2]
    coefficient = exp(res[1])
    (coefficient, order)
  end