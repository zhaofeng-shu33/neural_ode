\documentclass{article}
\usepackage{amsmath}
\usepackage{url}
\usepackage{graphicx}
\usepackage{subcaption}
\title{ODE Solver Step Length Auto Selection}
\author{Feng Zhao}

\begin{document}
\maketitle

Consider a standard ODE problem
\begin{align}
\frac{du}{dt} & = f(t, u) \notag\\
u(t_0) &= u_0
\end{align}
Many solvers are available to obtain numerical solutions of $u(t)$ for an interval $[t_0, t_{\max}]$.
For explicit methods, they use the following scheme to update $u_n$, which is the approximated value
of $u(t_n)$.
\begin{align}
u_{n+1} &= u_n + h_n\psi(t_n, u_n, h_n) \\
t_{n+1} &= t_n + h_n
\end{align}
\begin{description}
\item[fix-stepsize method] $h_n=h$ is pre-specified
\item[adaptive method] $h_{n+1} = g(t_n, u_n, h_n)$ is adjusted at each step.
\end{description}
In the above description of adaptive method, we focus on the simplied case when only the current state $(t_n, u_n, h_n)$
is used to obtain the new step size $h_{n+1}$. This is a local adaptive method, and relatively computational
efficient. 

To obtain a good form $h_{n+1} = g(t_n, u_n, h_n)$ such that the local error $|u(t_{n+1}) - u_{n+1}|$ decreases if new step length
is used. We need:
\begin{enumerate}
\item An error estimator of $\textrm{err}_n(h) \approx u(t_{n+1}) - u_{n+1}$, since $u(t)$ is unknown
\item A method  $h_{n+1} = g(t_n, u_n, h_n)$ to reduce $\textrm{err}_n(h) $
\end{enumerate}
In this report, we focus on the adjustment of step lengths of RK solvers.
Suppose $\psi(t_n, u_n, h_n)$ describes a RK solver with order $p$.
A common practice for $\textrm{err}_n(h) $ is to use a higher or lower order RK method to obtain $\tilde{u}_{n+1}$ to replace $u(t_{n+1})$. That is $\textrm{err}_n(h) = \tilde{u}_{n+1} - u_{n+1}
= h(\tilde{\psi}(t_n, u_n, h_n) - \psi(t_n, u_n, h_n))$ where $\tilde{\psi}(t_n, u_n, h_n)$ describes
an RK solver with order $\hat{p}$.

Another method to estimator $\textrm{err}_n(h) $ is use $u_n, u_{n+1}, u'_n = f(t_n, u_n),
u'_{n+1} = f(t_{n+1}, u_{n+1})$ to construct an interpolation curve $S(t)$ on the interval
$[t_n, t_{n+1}]$. Then we estimate the residual $r(t) = S'(t) - f(t, S(t))$, the
local error is chosen as $\Delta t||r(t)||$ \cite{shampine}.

\section{Traditional Method to adjust the steps}
Ceschino proposed the method using the multiplication factor $(\frac{1}{\textrm{err}_n(h)})^{1/(\tilde{p}+1)}$ to adjust
the step length, where $\tilde{p}=\min(p, \hat{p})$ is the smaller order of the two methods in comparison,
and $\textrm{err}_n(h)$ is the normalized error (normalized by the prescribed error threshold) \cite{ceschino}.
The formula can also be understood as $\textrm{err}_n(h)=O(h^{\tilde{p}+1})$
while the controlled method has $\textrm{err}_n(qh)=O(q^{\tilde{p}+1}h^{\tilde{p}+1}) \approx 1$
 %Fehlberg (1968) uses this method on a concrete scheme of RK method \cite{fehlberg}.
Then
we choose the new step length $qh$:
\begin{equation}\label{eq:icontroller}
q = (1/\textrm{err}_n(h))^{1/(\tilde{p}+1)}= \left(\frac{\epsilon}{| \tilde{u}_{n+1} - u_{n+1}|}\right)^{\frac{1}{\tilde{p}+1}}
\end{equation}
where $\epsilon$ is the error threshold.
\begin{enumerate}
\item If $\textrm{err}_n(h) < 1$, accept the new step length $h_{n+1} = qh_n$ and update $t_{n+1} = t_n + h_n$;
\item If $\textrm{err}_n(h) > 1$, repeat this step
\end{enumerate}

My initial idea:  solve the equation $|\textrm{err}_n(qh)|=\epsilon$ to obtain the propotional parameter $q$, where $\textrm{err}_n(h) 
= h(\tilde{\psi}(t_n, u_n, h_n) - \psi(t_n, u_n, h_n))$.

\begin{enumerate}
\item When $q=0$, $|\textrm{err}_n(qh)| = 0$;
\item When $q$ is large, $|\textrm{err}_n(qh)|> \epsilon$.
\end{enumerate}

We use Newton's method to solve $\textrm{err}^2_n(qh) - \epsilon^2=0$.
\begin{equation}
q_{\textrm{new}} = q_0 - \frac{\textrm{err}^2_n(q_0 h) - \epsilon^2}{2\textrm{err}_n(q_0 h)
[\frac{d\tilde{\psi}(t_n, u_n, qh_n)}{dq} -
\frac{d\psi(t_n, u_n,q h_n)}{dq}
]_{q=q_0}}
\end{equation}
where choose $q_0=1$.
Below we illustrate my idea using Midpoint scheme embedding Euler.
\begin{align}
k_1 &= f(t_n, u_n) \\
k_2 & = f(t_n + \frac{1}{2} h, u_n + \frac{1}{2}h k_1) \\
\textrm{err}_n(h) & = h(k_2 - k_1) = O(h^2)
\end{align}
We consider the derivatives $\frac{d \textrm{err}_n(qh) }{d q}$, after some calculation,
we get:
\begin{equation}
\frac{d \textrm{err}_n(qh) }{d q}
= \textrm{err}_n(h) + \frac{qh^2}{2} [\frac{\partial f}{\partial t}(t_n + \frac{1}{2}qh, u_n +
\frac{1}{2}hk_1) 
- k_1\frac{\partial f}{\partial u}(t_n + \frac{1}{2}qh, u_n +
\frac{1}{2}hk_1)]
\end{equation}
Notice that the second term in the above equation is $O(h^3)$ while $\textrm{err}_n(h) = O(h^2)$.
Therefore, we make the approximation
\begin{equation}\label{eq:approx}
\frac{d \textrm{err}_n(qh) }{d q} \approx  \textrm{err}_n(h)
\end{equation}
Then $q_{\textrm{new}} = 1 - \frac{\textrm{err}^2_n(h) - e^2}{2\textrm{err}_n(h)\frac{d \textrm{err}_n(qh) }{d q}\Big\vert_{q=1}}\approx \frac{1}{2} + \frac{e^2}{2 \textrm{err}^2_n(h)}$.
From this formula, if $ \textrm{err}_n(h) < e$, $q_{\textrm{new}} > 1$ and we increase the step length;
if $\textrm{err}_n(h) > e$, we decrease the step length $q_{\textrm{new}} < 1$ and repeat the current
step iteration.

Compared with Ceschino method, we use a different way to calculate $q$ thanks to \eqref{eq:approx}. 
$q_{\textrm{new}} = \frac{1}{2} + \frac{e^2}{2 \textrm{err}^2_n(h)}$ does not contain the derivative information, and the computational cost is the same with that of Ceschino.
%If $\frac{\partial f}{\partial t}$
%is required, auto-differential can be used for numerical differentiation (has additional computation cost).

Can our formulation improves the performance of the solver? (it needs less time steps to achieve the given
accuracy)
%\subsection{Runge-Kutta-Fehlberg Method}
\section{Experiments}
In Julia programming language, \texttt{Midpoint} scheme is implemented using embedding Euler to estimator error while the classical RK4 uses the residual method for error estimation.
Both of their adaptive versions use \texttt{IController}
(given in \eqref{eq:icontroller}).

Example 1: Consider a planar spiral with analytical expression in polar coordinate
$r=bt$. The ODE in Cartesian system
is:
\begin{align}
\frac{dx}{dt} &= b\cos t - y \\    
\frac{dy}{dt} &= b\sin t + x
\end{align}
with initial condition $x(t_0)=0, y(t_0)=0, t_0=0$
We can first solve out $x=bt\cos t, y=bt\sin t$.
We also consider the case when the inverse curve starts from $(2\pi, 0)$ and approaches
the origin (i-spiral).
The figure is given in Fig. \ref{fig:spiral_curve}
\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/spiral_time.eps}
    \caption{$x(t), y(t)$}\label{fig:time_spiral_curve}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/spiral.eps}
        \caption{$f(x,y)=0$}\label{fig:positional_spiral_curve}
    \end{subfigure}
        \caption{}\label{fig:spiral_curve}
    \end{figure}

We compute four cases,
given in Fig. \ref{fig:4cases}.

How to explain the phenomenon? Initially, adaptive method performs better than the fixed step method.
The accumulation of error causes the local error estimation less precise,
and the decision to adjust steps
based on such estimation has less sense.
Therefore, the adaptive method has worse performance compared
with the fixed step alternative.
\begin{figure}[!ht]
\centering
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{error_compare_midpoint.png}
\caption{Midpoint for spiral}\label{fig:com}
\end{subfigure}~
\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{error_compare_rk4.png}
    \caption{RK4 for i-spiral}\label{fig:com2}
\end{subfigure}\\
\begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{error_compare_midpoint_inverse.png}
    \caption{Midpoint for spiral}\label{fig:com_inv}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{error_compare_rk4_inverse.png}
        \caption{RK4 for i-spiral}\label{fig:com2_inv}
    \end{subfigure}
    \caption{}\label{fig:4cases}
\end{figure}

However, for more complex curves such as Brusselator,
whose curve is described by
\begin{align}
    y'_1 &= 1+ y_1^2 y_2 - 4y_1 \\
    y'-2 &=  3y_1 - y_162 y_2 
\end{align}
with initial values $y_1(0) = 1.5, y_2(0)=3$. The time interval is $[0,20]$.

The figure of this problem is given in Fig. \ref{fig:brusselator_curve}
\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{fig/spiral_time.eps}
    \caption{$x(t), y(t)$}\label{fig:time_spiral_curve}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/spiral.eps}
        \caption{$f(x,y)=0$}\label{fig:positional_spiral_curve}
    \end{subfigure}
        \caption{}\label{fig:brusselator_curve}
    \end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{error_compare_brusselator_midpoint.png}
    \caption{Midpoint for Brusselator}\label{fig:b_com}
    \end{subfigure}~
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{error_compare_brusselator_rk4.png}
        \caption{RK4 for Brusselator}\label{fig:b_com2}
    \end{subfigure}
        \caption{}\label{fig:2cases}
    \end{figure}

From Fig. \ref{fig:2cases}, we can see that the adaptive method is better with
some time overhead.
\bibliographystyle{plain}
\begin{thebibliography}{9}
\bibitem{ceschino} Ceschino, Francis. "Modification de la longueur du pas dans l’intégration numérique par les méthodes à pas liés." Chiffres 4 (1961): 101-106.
\bibitem{fehlberg} Fehlberg, Erwin. Low-order classical Runge-Kutta formulas with stepsize control and their application to some heat transfer problems. Vol. 315. National aeronautics and space administration, 1969.
\bibitem{shampine} Shampine, Lawrence F. "Error estimation and control for ODEs." Journal of Scientific Computing 25.1 (2005): 3-16.
\end{thebibliography}
\end{document}
