import os
import argparse
import pickle

import numpy as np
from scipy.io import savemat, loadmat

import matplotlib.pyplot as plt
from datetime import datetime
import torch
from torch import nn
from torch.utils.data import DataLoader

from rk import RKF4, Midpoint, BS3
from problems import LotkaVolterraProblem, SpiralProblem, BrusselatorProblem
from visualization import plot_log_h_versus_b, plot_log_h_versus_log_err, plot_log_h_versue_u,\
    plot_log_h_versue_t_max

METHOD_LIST = ['Midpoint', 'BS3', 'RKF4']
def get_problem_instance(problem_name):
    if problem_name == 'Spiral':
        return SpiralProblem()
    elif problem_name == 'Brusselator':
        return BrusselatorProblem()
    return LotkaVolterraProblem()

def init_normal(m):
    if type(m) == nn.Linear:
        nn.init.normal_(m.weight)
        nn.init.normal_(m.bias)

class step_size_fitter(nn.Module):
    def __init__(self, min_h, input_dim):
        super(step_size_fitter, self).__init__()
        # self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(input_dim, 3),
            nn.ReLU(),
            nn.Linear(3, 1),
            nn.ReLU()
        ) # 22 trainable variables
        self.linear_relu_stack.apply(init_normal)
        self.min_h = min_h
    def forward(self, x):
        # to do: change 1e-6 to exp(h_log_range[0])
        log_h = torch.log(torch.exp(torch.tensor(self.min_h)) + self.linear_relu_stack(x))
        return log_h

class step_size_fitter_with_prior(nn.Module):
    def __init__(self, input_dim):
        super(step_size_fitter_with_prior, self).__init__()
        # self.flatten = nn.Flatten()
        self.linear_tanh_stack = nn.Sequential(
            nn.Linear(input_dim - 1, 2),
            nn.Tanh(),
            nn.Linear(2, 1)
        ) # 13 trainable variables
        self.variable = nn.Parameter((torch.randn(())), requires_grad=True)
        self.linear_tanh_stack.apply(init_normal)
        self.input_dim = input_dim
    def forward(self, x):
        # to do: change 1e-6 to exp(h_log_range[0])
        log_err = x[:, (self.input_dim-1):self.input_dim]
        coeff = self.linear_tanh_stack(x[:, :(self.input_dim-1)])
        log_h = self.variable * log_err + coeff
        return log_h

class step_size_fitter_linear_model(nn.Module):
    def __init__(self, input_dim):
        super(step_size_fitter_linear_model, self).__init__()
        # self.flatten = nn.Flatten()
        self.linear_stack = nn.Sequential(
            nn.Linear(input_dim - 1, 1)
        )
        self.linear_stack.apply(init_normal)
        self.variable = nn.Parameter((torch.randn(())), requires_grad=True)
        self.input_dim = input_dim
        # parameters number: input_dim + 1
    def forward(self, x):
        # to do: change 1e-6 to exp(h_log_range[0])
        log_err = x[:, (self.input_dim-1):self.input_dim]
        coeff = self.linear_stack(torch.log(x[:, :(self.input_dim-1)]))
        log_h = self.variable * log_err + coeff
        return log_h

def train_loop(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    for batch, (X, y) in enumerate(dataloader):
        # Compute prediction and loss
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    loss = loss.item()
    print(f"Train loss: {loss:>7f}")

def test_loop(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss = 0

    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X)
            test_loss += loss_fn(pred, y).item()

    test_loss /= num_batches
    print(f"Test loss: {test_loss:>8f} \n")
    return test_loss

class Dataset(torch.utils.data.Dataset):
    'Characterizes a dataset for PyTorch'
    def __init__(self, size=6400, method='RKF4', problem='Spiral', save=True, load=True):
        'Initialization'
        file_name = f'build/xy-{problem}-{method}-{size}.mat'
        if load and os.path.exists(file_name):
            self.x, self.y = self.get_data(problem, method, size)
            return
        _method = None
        if method == 'Midpoint':
            _method = Midpoint
        elif method == 'BS3':
            _method = BS3
        elif method == 'RKF4':
            _method = RKF4
        elif method == 'All':
            # generate data by combining others            
            encoding_len = len(METHOD_LIST)
            x_list = []
            y_list = []
            for i, m in enumerate(METHOD_LIST):
                x, y = self.get_data(problem, m, size)
                x_pre = np.zeros([size, encoding_len], dtype=np.float32)
                x_pre[:, i] = 1
                x_list.append(np.hstack((x_pre, x)))
                y_list.append(y)
            self.x = np.vstack(x_list)
            self.y = np.vstack(y_list)
        else:
            raise ValueError(f"{method} not supported")
        if method != 'All':
            _problem = get_problem_instance(problem)
            self.x, self.y = _problem.get_batch(size=size, method=_method, verbose=True)
            self.y = self.y.reshape([size, 1])
        if save:
            dic = {'x': self.x, 'y': self.y}
            savemat(file_name, dic)

    @staticmethod
    def get_data(problem, method, size):
        file_name = f'build/xy-{problem}-{method}-{size}.mat'
        dic = loadmat(file_name)
        if method == 'All':
            _size = 3 * size
        else:
            _size = size
        return dic['x'], dic['y'].reshape([_size, 1])

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.y)

    def __getitem__(self, index):
        'Generates one sample of data'
        x = self.x[index,:]
        y = self.y[index,:]
        return x, y


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--regenerate', default=False, const=True, nargs='?')        
    parser.add_argument('--retrain', default=False, const=True, nargs='?')
    parser.add_argument('--verbose', default=False, const=True, nargs='?')
    parser.add_argument('--fresh_train', default=False, const=True, nargs='?')
    parser.add_argument('--ode_method', default='RKF4', choices=['RKF4', 'Midpoint', 'BS3', 'All'])
    parser.add_argument('--problem', default='Spiral', choices=['Spiral', 'LotkaVolterra', 'Brusselator'])
    parser.add_argument('--epoch', type=int, default=100)
    parser.add_argument('--batchsize', type=int, default=64)
    parser.add_argument('--lr', type=float, default=1e-3)
    parser.add_argument('--train_size', type=int, default=6400)
    parser.add_argument('--test_size', type=int, default=1280)
    parser.add_argument('--model', default='data-driven', choices=['with-prior', 'data-driven', 'linear'])

    args = parser.parse_args()
    train_size = args.train_size
    test_size = args.test_size
    problem = get_problem_instance(args.problem)
    input_dim = problem.input_dim
    if args.ode_method == 'All':
        input_dim += len(METHOD_LIST)
    if args.model == 'data-driven':
        model = step_size_fitter(problem.h_log_range[0], input_dim)
    elif args.model == 'linear':
        model = step_size_fitter_linear_model(input_dim)
    else:
        model = step_size_fitter_with_prior(input_dim)
    current_date_str = datetime.now().strftime('%Y-%m-%d')
    model_file_name = f'build/model-{args.model}-{args.ode_method}-{args.problem}-{current_date_str}-{train_size}.pth'

    if args.retrain:
        train_data = Dataset(size=train_size, method=args.ode_method, problem=args.problem, load=(not args.regenerate))
        test_data = Dataset(size=test_size, method=args.ode_method, problem=args.problem, load=(not args.regenerate))
        train_dataloader = DataLoader(train_data, batch_size=args.batchsize, shuffle=True)
        test_dataloader = DataLoader(test_data, batch_size=args.batchsize)


        learning_rate = args.lr
        optimizer = torch.optim.RMSprop(model.parameters(), lr=learning_rate)
        loss_fn = nn.MSELoss()
        if os.path.exists(model_file_name) and args.fresh_train is False:
            model.load_state_dict(torch.load(model_file_name))
        epochs = args.epoch
        for t in range(epochs):
            print(f"Epoch {t+1}\n-------------------------------")
            train_loop(train_dataloader, model, loss_fn, optimizer)
            test_loss = test_loop(test_dataloader, model, loss_fn)
            # see https://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVR.html#sklearn.svm.LinearSVR.score
        R2_score = 1- len(test_data) * test_loss / np.linalg.norm(test_data.y - np.mean(test_data.y)) ** 2
        print(f"R2 score: {R2_score:>8f} \n")
        # save model data
        parameter_dic = model.state_dict()
        torch.save(parameter_dic, model_file_name)
        if args.model == 'with-prior':
            dic = {}
            for k, v in parameter_dic.items():
                if isinstance(v, torch.Tensor):
                    npy_array = v.detach().numpy()
                    dic[k] = npy_array.tolist()
            with open(f'build/model-npy-{args.problem}-{args.ode_method}.pickle', 'wb') as f:
               pickle.dump(dic, f)
    else:
        _method = RKF4
        _err_exp = -10
        if args.ode_method == 'Midpoint':
            _method = Midpoint
            _err_exp = -3 # empirical choice
        elif args.ode_method == 'BS3':
            _method = BS3
            _err_exp = -5 # empirical choice
        model_parameter_dic = torch.load(model_file_name)
        if args.verbose:
            print(model_parameter_dic)
        
        model.load_state_dict(model_parameter_dic)
        plot_log_h_versus_log_err(problem, model, 0, f'build/nn_{args.ode_method}_{args.model}_h_error.eps', method=_method)
        plot_log_h_versue_t_max(problem, model, 1, f'build/nn_{args.ode_method}_{args.model}_tmax_error.eps', method=_method, err_exp=_err_exp)
        plot_log_h_versus_b(problem, model, 2, f'build/nn_{args.ode_method}_{args.model}_b_error.eps', method=_method, err_exp=_err_exp)
        plot_log_h_versue_u(problem, model, 3, f'build/nn_{args.ode_method}_{args.model}_u_0_x_error.eps', method=_method, err_exp=_err_exp)
        plt.show()

