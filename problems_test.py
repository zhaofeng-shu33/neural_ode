import pytest

import numpy as np
from scipy.integrate import solve_ivp

from problems import SpiralProblem, LotkaVolterraProblem
from rk import BS3

def test_spiral_get_batch():
    p = SpiralProblem()
    p.get_batch(size=2)

def test_lotka_get_abs_error():
    p = LotkaVolterraProblem()
    t = np.linspace(0, 1)
    u_0 = [1.0, 1.0]
    theta = [1.5,1,3,1]
    embedded_ode_func = lambda t, y: p.ode_func(t, y, u_0, theta)
    result = solve_ivp(embedded_ode_func, [0, 1], u_0, method=BS3, step=1e-2)
    y_old = np.zeros(50)
    e1 = p.abs_error(u_0, theta, result.t, result.y)
    assert e1 < 1e-2

def test_lotka_get_batch():
    p = LotkaVolterraProblem()
    p.get_batch(size=2)

