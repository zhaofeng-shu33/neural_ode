## Steps
* Uninstall `OrdinaryDiffEq`
* Install the modified `OrdinaryDiffEq` package (containing the required `struct NController`)
```Julia
pkg> add https://github.com/zhaofeng-shu33/OrdinaryDiffEq.jl#ncontroller
```
