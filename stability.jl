# (2.27) of Solving ordinary differential equations II
using OrdinaryDiffEq
using DiffEqDevTools
function f(y, p, t)
    dy1 = -2000 * (cos(t) * y[1] + sin(t) * y[2] + 1)
    dy2 = -2000 * (-sin(t) * y[1] + cos(t) * y[2] + 1)
    [dy1,dy2]
end
tspan = (0.0,1.57)
u0 = [1.0,0.0]
prob = ODEProblem(f,u0,tspan)
sol = solve(prob, DP5(), gamma=0.9, controller=IController())
println(sol.destats)
sol2 = solve(prob, DP5(), gamma=0.7, controller=NController())
println(sol2.destats)
