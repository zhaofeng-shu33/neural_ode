using Test
include("utility.jl")
include("problems.jl")

function test_get_coefficient_and_order()
    f(u,p,t) = 1.01*u
    u0 = 1/2
    tspan = (0.0,1.0)
    prob = ODEProblem(f,u0,tspan)
    sol = solve(prob, Tsit5(), reltol=1e-14, abstol=1e-14)
    coefficient, order = get_coefficient_and_order(sol, prob, Midpoint)
    new_mid = Midpoint(coefficient, order)
    new_sol = solve(prob, new_mid, abstol=1e-5, adaptive=false)
    ≈(order, 2.0, atol=0.01) && appxtrue(new_sol, sol).errors[:final] < 1e-4
end
function test_use_model_predict_bs3()
    prob = prob_spiral
    sol = solve(prob, Tsit5(), reltol=1e-14, abstol=1e-14)
    coefficient, order = get_coefficient_and_order(sol, prob, BS3; use_prior=true)
    new_bs3 = BS3(coefficient, order)
    new_sol = solve(prob, new_bs3, abstol=1e-5, adaptive=false)
    ≈(order, 3.0, atol=0.05) && appxtrue(new_sol, sol).errors[:final] < 1e-4
end
function test_use_model_predict_rkf4()
    prob = prob_spiral
    sol = solve(prob, Tsit5(), reltol=1e-14, abstol=1e-14)
    _, order = get_coefficient_and_order(sol, prob, RKF4; use_prior=true)
    ≈(order, 4.0, atol=0.05)
end
function test_use_model_predict_midpoint()
    prob = prob_spiral
    sol = solve(prob, Tsit5(), reltol=1e-14, abstol=1e-14)
    _, order = get_coefficient_and_order(sol, prob, Midpoint; use_prior=true)
    ≈(order, 2.0, atol=0.05)
end
@test test_get_coefficient_and_order()
@test test_use_model_predict_bs3()
@test test_use_model_predict_rkf4()
@test test_use_model_predict_midpoint()