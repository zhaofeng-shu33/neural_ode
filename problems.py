import numpy as np
from scipy.integrate import solve_ivp

from rk import RK4, Midpoint, BS3
class DataGenerator:
    def __init__(self):
        self.theta_range = None
        self.u_0_range = None
        self.h_log_range = None
        self.t_max_range = None
    @staticmethod
    def ode_func(t, y, u_0, theta):
        # theta: parameter of the ODE function
        raise NotImplementedError()
    def abs_error(self, u_0, theta, t, y_old, method=RK4):
        # no analytical solution, needs approximation
        embedded_ode_func = lambda t, y: self.ode_func(t, y, u_0, theta)
        t_span = [0, max(t)]
        h = self.h_log_range[0]
        result = solve_ivp(embedded_ode_func, t_span, u_0, method=RK4, dense_output=True, step=np.exp(h - 0.5))
        y_true = result.sol(t)
        err_inf = np.max(np.linalg.norm(y_true - y_old, axis=0))
        if err_inf < 2e-14:
            raise ValueError("err is too small")
        return err_inf

    def get_batch(self, size=64, method=RK4, verbose=False):
        '''
        size: number of samples to generate
        method: ODE solver to obtain error given the parameters
        '''
        parameter_range = []
        parameter_range.extend(self.theta_range)
        parameter_range.extend(self.u_0_range)
        parameter_range.append(self.t_max_range)
        parameter_range.append(self.h_log_range)
        theta_dim = len(self.theta_range)
        u_0_dim = len(self.u_0_range)
        # generate uniform random samples
        para_array = np.zeros([len(parameter_range), size])
        for i, para in enumerate(parameter_range):
            para_array[i, :] = np.random.uniform(para[0], para[1], size)
        batch_y = np.zeros(size)
        j = 0
        while j < size:
            theta = para_array[0:theta_dim, j]
            u_0 = para_array[theta_dim:(theta_dim + u_0_dim), j]
            t_span = [0, para_array[theta_dim + u_0_dim, j]]
            step_size_h = para_array[theta_dim + u_0_dim + 1, j]
            embedded_ode_func = lambda t, y: self.ode_func(t, y, u_0, theta)            
            result = solve_ivp(embedded_ode_func, t_span, u_0, method=method, step=np.exp(step_size_h))
            if np.any(np.isinf(result.y)) or np.any(np.isnan(result.y)): # avoid non-convergent results
                para_array[theta_dim + u_0_dim + 1, j] -= 1 # decrease the step size and re-solve the ODE
                continue
            err = self.abs_error(u_0, theta, result.t, result.y)
            if err > 1:
                para_array[theta_dim + u_0_dim + 1, j] -= 1
                continue
            if verbose and j % 100 == 0:
                print(f"generate process: [{j:>5d}/{size:>5d}]")
            batch_y[j] = step_size_h
            para_array[theta_dim + u_0_dim + 1, j] = np.log(err)
            j += 1
        batch_x = para_array.T
        return batch_x.astype(np.float32), batch_y.astype(np.float32)

class SpiralProblem(DataGenerator):
    def __init__(self):
        self.theta_range = [[0.5, 4]]
        U_MIN = 0.001
        U_MAX = 10
        self.u_0_range = [[U_MIN, U_MAX], [U_MIN, U_MAX]]
        self.h_log_range = [-4, -0.2]
        self.t_max_range = [1.0* np.pi, 8 * np.pi]
        self.input_dim = 5
        self.u_0 = [U_MIN, U_MIN] # used for plot
        self.t_max = 2 * np.pi # used for plot
        self.theta = [1.0]
    @staticmethod
    def ode_func(t, y, u_0, theta):
        # theta: parameter of the ODE function
        return [theta[0] * np.cos(t) - y[1] + u_0[1], theta[0] * np.sin(t) + y[0] - u_0[0]]
    @staticmethod
    def abs_error(u_0, theta, t, y_old):
        # calculate global absolute error L_{\infty} norm
        x = u_0[0] + theta[0] * t * np.cos(t)
        y = u_0[1] + theta[0] * t * np.sin(t)
        y_true = np.vstack((x, y))
        return np.max(np.linalg.norm(y_true - y_old, axis=0))


class LotkaVolterraProblem(DataGenerator):
    def __init__(self):
        theta_max = 2
        self.theta_range = [[0.5, theta_max], [0.5, theta_max], [0.5, theta_max], [0.5, theta_max]]
        U_MAX = 10
        self.u_0_range = [[0.01, U_MAX], [0.01, U_MAX]]
        self.h_log_range = [-5, -1]
        self.t_max_range = [1.0, 15]
        self.input_dim = 8
        self.u_0 = [1.0,1.0] # used for plot
        self.t_max = 10.0 # used for plot
        self.theta = [1.5,1,3,1]
    @staticmethod
    def ode_func(t, y, u_0, theta):
        # theta: parameter of the ODE function
        return [y[0] * (theta[0] - theta[1] * y[1]), - y[1] * (theta[2] - theta[3] * y[0])]


class BrusselatorProblem(DataGenerator):
    def __init__(self):
        self.theta_range = [[0.5, 2], [1, 5]]
        self.u_0_range = [[0.5, 6], [0.5, 6]]
        self.h_log_range = [-5, -1]
        self.t_max_range = [4.0, 30]
        self.input_dim = 6
        self.u_0 = [1.5, 3.0] # used for plot
        self.t_max = 20.0 # used for plot
        self.theta = [1.5,3]
    @staticmethod
    def ode_func(t, y, u_0, theta):
        # theta: parameter of the ODE function
        return [theta[0] + y[0] ** 2 * y[1] - (theta[1] + 1) * y[1], theta[1] *  y[1] - y[0] ** 2 * y[1] ]
