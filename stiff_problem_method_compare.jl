using ParameterizedFunctions, OrdinaryDiffEq,
      Plots
using DiffEqDevTools
using LinearAlgebra
using Sundials
rober = @ode_def begin
    dy₁ = -k₁*y₁+k₃*y₂*y₃
    dy₂ =  k₁*y₁-k₂*y₂^2-k₃*y₂*y₃
    dy₃ =  k₂*y₂^2
  end k₁ k₂ k₃
  prob = ODEProblem(rober,[1.0,0.0,0.0],(0.0,1e5),[0.04,3e7,1e4])
  sol = solve(prob,CVODE_BDF(),abstol=1/10^14,reltol=1/10^14)
  test_sol = TestSolution(sol)
  
  abstols = 1.0 ./ 10.0 .^ (7:8)
  reltols = 1.0 ./ 10.0 .^ (3:4);
  
  setups = [Dict(:alg=>Rosenbrock23())
            Dict(:alg=>TRBDF2())
            Dict(:alg=>RadauIIA5())
            ]
  
names = [
    "Julia: Rosenbrock23"
    "Julia: TRBDF2"
    "Julia: radau"
]
  
wp = WorkPrecisionSet(prob,abstols,reltols,setups;
                        names = names,print_names = true,
                        dense=false,verbose = false,
                        save_everystep=false,appxsol=test_sol,
                        maxiters=Int(1e5))
plot(wp,title="Stiff 1: ROBER", legend=:bottomleft)
savefig("compare_stiff.png")