import argparse
import numpy as np
from rk import BS3, RKF4, DOPRI5
from scipy.integrate import solve_ivp

# Problem from page 26 of [1]
# [1]: Wanner, Gerhard, and Ernst Hairer. Solving ordinary differential equations II. Vol. 375. Springer Berlin Heidelberg, 1996.
def stiff_problem(t, y):
    y1 = -2000 * (np.cos(t) * y[0] + np.sin(t) * y[1] + 1)
    y2 = -2000 * (-np.sin(t) * y[0] + np.cos(t) * y[1] + 1)
    return [y1, y2]


u_0 = [-1, -1]
t_max = np.pi / 2
_max_nfev = 10000

sol_true = solve_ivp(stiff_problem, [0,t_max], u_0, method='Radau', atol=1e-12, rtol=1e-9, dense_output=True)
# 10-9

def pi_objective_function(beta_1, beta_2): # needs to be minimized to obtain T_min
    sol = solve_ivp(stiff_problem,
                [0, t_max], u_0, method=BS3, controller='PIController',
                beta_1=beta_1, beta_2=beta_2, max_nfev=_max_nfev)
    return sol.nfev

def pi_objective_function_pair(beta_1, beta_2):
    sol = solve_ivp(stiff_problem,
                [0, t_max], u_0, method=BS3, controller='PIController',
                beta_1=beta_1, beta_2=beta_2, max_nfev=_max_nfev)
    y_true = sol_true.sol(sol.t)
    err = np.max(np.linalg.norm(sol.y - y_true, axis=0))
    return (err, sol.nfev)

def verify(num=1, controller='PIController'):
    sol = solve_ivp(stiff_problem,
                [0, t_max], u_0, method=BS3, controller=controller,
                beta_1=None, beta_2=None, max_nfev=_max_nfev)
    y_true = sol_true.sol(sol.t)
    print(np.linalg.norm(sol.y - y_true))
    print(len(sol.t))
    print(sol.nfev)
    import matplotlib.pyplot as plt
    plt.figure(num)
    plt.plot(sol.t, sol.y[0])
    plt.plot(sol.t, sol.y[1])
    plt.xlabel('t')
    plt.ylabel('y')
    plt.savefig(f'build/{controller}_stiff.eps')
    plt.show()
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--action', choices=['verify', 'sample'], default='verify')
    args = parser.parse_args()
    if args.action == 'verify':
        verify()
        verify(2, 'IController')
    elif args.action == 'sample':
        print(pi_objective_function_pair(0.33, 0.01))
        print(pi_objective_function_pair(None, None))
