# use exported model parameters from pytorch

using Flux

function build_model(pickled_data)
    tmp = pickled_data["linear_tanh_stack.0.weight"]
    input_dim = 1 + length(tmp[1])
    mlp = Chain(
                Dense(input_dim - 1, 2, tanh),
                Dense(2, 1))

    for i=1:length(tmp)
        mlp[1].W[i,:] .= tmp[i] 
    end
    tmp = pickled_data["linear_tanh_stack.2.weight"]
    for i=1:length(tmp)
        mlp[2].W[i,:] .= tmp[i] 
    end

    mlp[1].b .= pickled_data["linear_tanh_stack.0.bias"]
    mlp[2].b .= pickled_data["linear_tanh_stack.2.bias"]
    variable = pickled_data["variable"]

    step_size_fitter_with_prior = function(x)        
            log_err = x[:, input_dim]
            coeff = mlp(transpose(x[:, 1:(input_dim-1)]))
            variable * log_err +  transpose(coeff)
        end
    return step_size_fitter_with_prior
end
