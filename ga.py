import array
import random

import numpy as np


from deap import base
from deap import creator
from deap import tools
from deap import algorithms

from controller_tuning import pi_objective_function, pi_objective_function_pair
creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", array.array, typecode='d', fitness=creator.FitnessMin)

toolbox = base.Toolbox()

# Problem definition
BOUND_LOW, BOUND_UP = 0.01, 1.0


NDIM = 2
def obj_func_global(individual):
    beta_1  = individual[0] 
    beta_2 = individual[1]
    return (pi_objective_function(beta_1, beta_2),)

def obj_func_constraint(T_star, individual):
    beta_1  = individual[0] 
    beta_2 = individual[1]
    err, T = pi_objective_function_pair(beta_1, beta_2)
    if T <= T_star:
        return (err,)
    else:
        return (np.inf,)

def uniform(low, up, size=None):
    try:
        return [random.uniform(a, b) for a, b in zip(low, up)]
    except TypeError:
        return [random.uniform(a, b) for a, b in zip([low] * size, [up] * size)]

toolbox.register("attr_float", uniform, BOUND_LOW, BOUND_UP, NDIM)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.attr_float)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)


toolbox.register("mate", tools.cxSimulatedBinaryBounded, low=BOUND_LOW, up=BOUND_UP, eta=20.0)
toolbox.register("mutate", tools.mutPolynomialBounded, low=BOUND_LOW, up=BOUND_UP, eta=20.0, indpb=1.0/NDIM)
toolbox.register("select", tools.selTournament, tournsize=3)

NGEN = 28 # number of generation
MU = 150
CXPB = 0.9

def get_min_E(T_min=None, seed=None, pop=None):
    random.seed(seed)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", np.min, axis=0)
    if T_min is None:
        toolbox.register("evaluate", obj_func_global)
    else:
        toolbox.register("evaluate", obj_func_constraint, T_min)
    if pop is None:
        pop = toolbox.population(n=MU)
    hof = tools.HallOfFame(1)
    pop, _ = algorithms.eaSimple(pop, toolbox, cxpb=CXPB, mutpb=0.9, ngen=NGEN, 
                                   stats=stats, halloffame=hof, verbose=True)

    return (pop, hof)

if __name__ == "__main__":
    pop, hof = get_min_E()
    beta_1, beta_2 = hof[0].tolist()
    err, T_min = pi_objective_function_pair(beta_1, beta_2)
    err_list = [err]
    beta_list = []
    beta_list.append(hof[0].tolist())
    T_list = np.linspace(T_min, 9000, num=10)
    for i in range(1, 10):
        print("Current iteration number: ", i)
        _, hof_i = get_min_E(T_list[i], pop=pop)
        beta_1_i, beta_2_i = hof_i[0].tolist()
        beta_list.append(hof_i[0].tolist())
        err_i, _ = pi_objective_function_pair(beta_1_i, beta_2_i)
        err_list.append(err_i)
    beta_1_list = [i[0] for i in beta_list]
    beta_2_list = [i[1] for i in beta_list]
    import matplotlib.pyplot as plt
    plt.figure(0)
    plt.scatter(T_list, beta_1_list)
    plt.scatter(T_list, beta_2_list)
    plt.figure(1)
    print(err_list)
    plt.scatter(T_list, np.log(err_list))
    plt.show()
